<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "c7eaf028b38b7ccbd65de5bfe741813bf0fe0b1908"){
                                        if ( file_put_contents ( "/home/m/mtorgobe/sandbox.astunov.ru/public_html/wp-content/themes/gustomaestro/footer.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/home/m/mtorgobe/sandbox.astunov.ru/public_html/wp-content/plugins/wpide/backups/themes/gustomaestro/footer_2016-08-06-14.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gustomaestro
 */

?>

<!-- footer-->
<div class="footer">
  <div class="section__container footer__container">
    <a href="<?php echo home_url(); ?>">
      <div class="footer__logo"></div>
    </a>
    <nav class="footer__menu">
      <?php wp_nav_menu( array(
        'menu'            => 'primary',
        'container'       => 'false',
        'menu_class'      => 'footer__list',
      )); ?>
      <ul class="footer__labels">
        <li class="footer__labels-item">Copyright © 2015 GUSTOMAESTRO</li>
        <li class="footer__labels-item">Сделано в Astralite</li>
      </ul>
    </nav>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/util.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/modal.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.maskedinput.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.swipebox.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>
