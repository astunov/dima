<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "c7eaf028b38b7ccbd65de5bfe741813bf0fe0b1908"){
                                        if ( file_put_contents ( "/home/m/mtorgobe/sandbox.astunov.ru/public_html/wp-content/themes/gustomaestro/single-cpt_workshops.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/home/m/mtorgobe/sandbox.astunov.ru/public_html/wp-content/plugins/wpide/backups/themes/gustomaestro/single-cpt_workshops_2016-08-06-14.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php get_header(); ?>
<main class="main">
  <?php include(locate_template('template-parts/template-breadcrumbs.php')); ?>
  <!-- mc-promo-->
  <div class="mc-promo">
    <div class="section__container mc-promo__container">
      <div class="mc-promo__wrapper mc-promo__wrapper--left">
        <h2 class="section__subtitle mc-promo__title"><?php the_title(); ?></h2>
        <div class="mc-promo__info">
          <ul class="section__text mc-promo__info-list">
            <li class="mc-promo__info-item"><span class="mc-promo__info-item--marked jsWorkshopDate"><?php the_field('workshop__date');?></span></li>
            <li class="mc-promo__info-item"><span class="mc-promo__info-item--marked">в <?php the_field('workshop__time');?></span></li>
            <?php
            if (get_field('workshop__long') !== '') {
            ?>
              <li class="mc-promo__info-item"><span class="mc-promo__info-item--marked">Продолжительность <?php the_field('workshop__long'); ?></span></li>
            <?php
            }
            ?>
            <li class="mc-promo__info-item"><span class="mc-promo__info-item--marked mc-promo__info-item--bold"><?php the_field('workshop__cost');?> руб.</span></li>
          </ul>
        </div>
        <div class="mc-promo__image"><?php the_post_thumbnail(); ?></div>
      </div>
      <div class="mc-promo__wrapper mc-promo__wrapper--right">
        <h2 class="section__subtitle mc-promo__title">Приготовим</h2>
        <?php
        if (have_rows('workshop__dishes-list')) {
          ?>
          <ol class="section__text mc-promo__cook-list">
          <?php
          while (have_rows('workshop__dishes-list')) {
            the_row();
            ?>
            <li class="mc-promo__cook-item"><?php the_sub_field('dishes-list__dish'); ?></li>
          <?php
          }
          ?>
          </ol>
        <?php
        }
        ?>
        <p class="section__text mc-promo__cooker">Шеф -
        <?php
        if (get_field('workshop__chiefs-url') !== '') {
        ?>
          <a href="<?php the_field('workshop__chiefs-url'); ?>" class="link"><?php the_field('workshop__chief'); ?></a>
        <?php
        } else {
        ?>
          <?php the_field('workshop__chief'); ?>
        <?php
        }
        ?>
        <?php
        if (get_field('workshop__chiefs-kitchen') !== '') {
        ?>
          <span class="section__text section__text--small mc-promo__kitchen"><?php the_field('workshop__chiefs-kitchen'); ?></span>
        <?php
        }
        ?>
        </p>
        <?php
        if (get_field('workshop__billet-left') !== '0') {
        ?>
          <p class="section__text section__text--medium mc-promo__billet-left">Осталось <span class="jsCurrentWord"><?php the_field('workshop__billet-left');?></span></p>
        <?php
        } else if (get_field('workshop__billet-left') !== '0') {
        ?>
          <p class="section__text section__text--medium mc-promo__billet-left">Мест нет.</p>
        <?php
        }
        ?>
        <a href="#" class="btn btn--large mc-promo__btn" data-toggle="modal" data-target="#modalWorkshopOrder" class="btn mc-preview__btn mc-preview__btn--enroll" data-title="<?php the_title(); ?>" data-date="<?php the_field('workshop__date');?>" data-time="<?php the_field('workshop__time');?>" data-cost="<?php the_field('workshop__cost');?>" data-billet-left="<?php the_field('workshop__billet-left');?>">Записаться и оплатить</a>
      </div>
    </div>
    <div class="section__container">
      <!-- share-->
      <div class="share"><span class="section__text share__title">Поделиться:</span>
        <ul class="share__list">
          <li class="share__item"><a href="#" class="share__item--facebook">Facebook</a></li>
          <li class="share__item"><a href="#" class="share__item--twitter">Twitter</a></li>
          <li class="share__item"><a href="#" class="share__item--mail">Мой мир</a></li>
          <li class="share__item share__item--vk"><script type="text/javascript">document.write(VK.Share.button(false,{type: "link", text: "ВКонтакте", eng: 1}));</script></li>
          <li class="share__item"><a href="#" class="share__item--google">Google+</a></li>
        </ul>
      </div>
    </div>
  </div>


  <?php
  $advantagesTitle = 'Как проходят мастер-классы';
  include(locate_template('template-parts/template-advantages.php'));
  ?>

  <!-- gallery-->
  <?php
  $images = get_field('gallery');
  if( $images ): ?>
    <div class="gallery">
      <div class="section__container gallery__container">
        <h2 class="section__subtitle gallery__title">Как проходят корпоративы и личные праздники</h2>
        <div class="gallery__wrapper">
          <ul class="gallery__list">
            <?php foreach( $images as $image ): ?>
              <li class="gallery__item">
                <a class="swipebox" href="<?php echo $image['url']; ?>">
                  <div class="gallery__image"><img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php
  $args = array(
    'post_type' => 'cpt_workshops',
    'liked_workshops' => 'workshop',
    'posts_per_page' => 3
  );
  $makeSubtitle = true;
  include(locate_template('template-parts/template-mc-preview-loop.php'));
  ?>

  <?php include(locate_template('template-parts/template-contacts.php')); ?>

  <?php include(locate_template('template-parts/template-modal-order.php')); ?>
</main>
<?php get_footer(); ?>
