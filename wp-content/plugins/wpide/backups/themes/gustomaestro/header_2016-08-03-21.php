<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "c7eaf028b38b7ccbd65de5bfe741813bb6c2352afa"){
                                        if ( file_put_contents ( "/home/m/mtorgobe/sandbox.astunov.ru/public_html/wp-content/themes/gustomaestro/header.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/home/m/mtorgobe/sandbox.astunov.ru/public_html/wp-content/plugins/wpide/backups/themes/gustomaestro/header_2016-08-03-21.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gustomaestro
 */

?><!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php the_title(); ?> | <?php bloginfo('name'); ?></title>
    <meta name="description" content="">
    <meta name="application-name" content="True">
    <meta name="HandheldFriendly" content="True">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta property="og:type" content="profile">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:url" content="">
    <meta property="og:site_name" content="">
    <link rel="apple-touch-icon" href="images/touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/touch-icon-ipad-retina.png">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"><!-- build:css -->
    <!-- <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/mycalendar.css"> -->
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/clndr.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/swipebox.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/main.css"><!-- endbuild -->

    <script type="text/javascript" src="//vk.com/js/api/share.js?93" charset="windows-1251"></script>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <?php wp_head(); ?>
  </head>
  <body><!--[if IE 8]>
    <p>Вы используете <strong>устаревшую версию</strong> браузера. Пожалуйста, <a href="http://browsehappy.com/">обновите браузер</a> на официальном сайте, чтобы просматривать полную версию сайта.</p><![endif]-->
    <!-- top-panel-->
    <section class="top-panel">
      <div class="section__container top-panel__container">
        <ul class="top-panel__social-list">
          <li class="top-panel__social-item"><a target="_blank" href="http://www.gustomaestro.ru/feed.php" class="top-panel__social-item--rss"></a></li>
          <li class="top-panel__social-item"><a target="_blank" href="https://vk.com/gustomaestro" class="top-panel__social-item--vk"></a></li>
          <li class="top-panel__social-item"><a target="_blank" href="https://www.facebook.com/gustospb?ref=hl" class="top-panel__social-item--facebook"></a></li>
          <li class="top-panel__social-item"><a target="_blank" href="https://www.instagram.com/gustorestaurant/" class="top-panel__social-item--instagram"></a></li>
          <li class="top-panel__social-item"><a target="_blank" href="http://goo.gl/Ra2iEb" class="top-panel__social-item--tripadvisor"></a></li>
          <li class="top-panel__social-item"><a target="_blank" href="https://goo.gl/WMmoKW" class="top-panel__social-item--foursquare"></a></li>
        </ul>
        <ul class="section__text section__text--small top-panel__contacts-list">
          <li class="top-panel__contacts-item top-panel__contacts-item--phone"><a target="_blank" href="tel:78129033732">+7 (812) 903 37 32</a></li>
          <li class="top-panel__contacts-item top-panel__contacts-item--address">Санкт-Петербург, Дегтярная ул., 1</li>
        </ul>
      </div>
    </section>
    <!-- header-->
    <header class="header">
      <div class="section__container header__container">
        <nav class="main-menu">
          <?php wp_nav_menu( array(
            'menu'            => 'header__left-menu',
            'container'       => 'false',
            'menu_class'      => 'main-menu__list main-menu__list--left',
          )); ?>
          <a href="<?php echo home_url(); ?>" class="main-menu__logo"></a>
          <div id="openMenu" data-open="false" class="menu__btn"><span></span><span></span><span></span></div>
          <?php wp_nav_menu( array(
            'menu'            => 'header__right-menu',
            'container'       => 'false',
            'menu_class'      => 'main-menu__list main-menu__list--right',
          )); ?>
          <?php wp_nav_menu( array(
            'menu'            => 'primary',
            'container'       => 'false',
            'menu_class'      => 'main-menu__list main-menu__list--tablet',
            'menu_id'         => 'mobileMenu',
          )); ?>
        </nav>
      </div>
    </header>
