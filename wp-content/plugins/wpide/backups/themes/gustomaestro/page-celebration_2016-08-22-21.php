<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "c7eaf028b38b7ccbd65de5bfe741813b3546a7a976"){
                                        if ( file_put_contents ( "/home/m/mtorgobe/sandbox.astunov.ru/public_html/wp-content/themes/gustomaestro/page-celebration.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/home/m/mtorgobe/sandbox.astunov.ru/public_html/wp-content/plugins/wpide/backups/themes/gustomaestro/page-celebration_2016-08-22-21.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php get_header(); ?>
<main class="main">
  <!-- promo-->
  <section class="promo promo--celebration" style="background: url(<?php the_field('promo__background'); ?>) no-repeat center/cover;">
    <div class="section__container promo__container">
      <h1 class="section__header promo__title"><?php the_title(); ?></h1>
    </div>
  </section>
  <!-- two-col-text-->
  <div class="two-col-text two-col-text--small-padding">
    <div class="section__container two-col-text__container">
      <p class="section__text two-col-text__text"><?php the_field('two-column__first'); ?></p>
      <p class="section__text two-col-text__text"><?php the_field('two-column__second'); ?></p>
    </div>
  </div>
  <!-- celebration-services-->
  <section class="celebration-services">
    <div class="section__container celebration-services__container">
      <ul class="celebration-services__list">
        <li class="celebration-services__item">
          <h3 class="section__subtitle celebration-services__title">Корпоративные мероприятия</h3>
          <div class="celebration-services__content">
            <p class="section__text celebration-services__text"><?php the_field('celebration__corporate-text'); ?></p>
            <a href="#calendar" class="section__text link">Посмотреть свободные даты</a>
          </div>
          <div class="celebration-services__offer">
            <?php
            if (have_rows('celebration__corporate-list')) {
            ?>
            <ul class="section__text celebration-services__service-list">
              <?php
              while (have_rows('celebration__corporate-list')) {
                the_row();
                ?>
                <li class="celebration-services__service-item"><?php the_sub_field('celebration__corporate-item'); ?></li>
              <?php
              }
              ?>
            </ul>
            <?php
            }
            ?>
            <a href="#" class="btn btn--large celebration-services__btn" data-toggle="modal" data-target="#modalOrderPreviewEvent">Заказать</a>
          </div>
        </li>
        <li class="celebration-services__item">
          <h3 class="section__subtitle celebration-services__title">Личные праздники</h3>
          <div class="celebration-services__content">
            <p class="section__text celebration-services__text"><?php the_field('celebration__personal-text'); ?></p>
            <a href="#calendar" class="section__text link">Посмотреть свободные даты</a>
          </div>
          <div class="celebration-services__offer">
            <?php
            if (have_rows('celebration__personal-list')) {
            ?>
            <ul class="section__text celebration-services__service-list">
              <?php
              while (have_rows('celebration__personal-list')) {
                the_row();
                ?>
                <li class="celebration-services__service-item"><?php the_sub_field('celebration__personal-item'); ?></li>
              <?php
              }
              ?>
            </ul>
            <?php
            }
            ?>
            <a href="#" class="btn btn--large celebration-services__btn" data-toggle="modal" data-target="#modalOrderPreviewEvent">Заказать</a>
          </div>
        </li>
      </ul>
    </div>
  </section>
  <!-- celebration-events-->
  <section class="celebration-events">
    <div class="section__container celebration-events__container">
      <h3 class="section__subtitle celebration-events__title">Организуем мероприятия</h3>
      <ul class="celebration-events__list">
        <li class="celebration-events__item">
          <div class="celebration-events__text"><span class="section__title celebration-events__item-title">Для взрослых</span></div><a href="<?php echo get_permalink(143); ?>"></a><img src="<?php the_field('celebration__image--adult'); ?>" alt="">
        </li>
        <li class="celebration-events__item">
          <div class="celebration-events__text"><span class="section__title celebration-events__item-title">Для детей</span></div><a href="<?php echo get_permalink(147); ?>"></a><img src="<?php the_field('celebration__image--children'); ?>" alt="">
        </li>
      </ul>
    </div>
  </section>

  <div class="calendar" id="calendar">
    <div class="section__container calendar__container">
      <h3 class="section__subtitle calendar__title calendar__title--left">Свободные даты</h3>
      <div class="cal1"></div>
      <a href="#" class="btn calendar__btn" data-toggle="modal" data-target="#modalOrderPreviewEvent">Заказать организацию мероприятия</a>
    </div>
  </div>

  <?php include(locate_template('template-parts/template-contacts.php')); ?>
</main>
<?php include(locate_template('template-parts/template.modal-order-preview-event.php')); ?>
<?php get_footer(); ?>
