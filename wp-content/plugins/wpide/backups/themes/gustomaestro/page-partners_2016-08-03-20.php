<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "c7eaf028b38b7ccbd65de5bfe741813bb6c2352afa"){
                                        if ( file_put_contents ( "/home/m/mtorgobe/sandbox.astunov.ru/public_html/wp-content/themes/gustomaestro/page-partners.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/home/m/mtorgobe/sandbox.astunov.ru/public_html/wp-content/plugins/wpide/backups/themes/gustomaestro/page-partners_2016-08-03-20.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php get_header(); ?>
<main class="main">
  <!-- partners-->
  <div class="partners">
    <div class="section__container partners__container">
      <h2 class="section__title partners__title">Партнёры</h2>
      <div class="partners__wrapper">
        <?php
        if (have_rows('partners__logo-list')) {
        ?>
        <ul class="partners__logo-list">
          <?php
          while (have_rows('partners__logo-list')) {
            the_row();
            ?>
            <li class="partners__item"><img src="<?php the_sub_field('partners__logo-image'); ?>" alt=""></li>
          <?php
          }
          ?>
        </ul>
        <?php
        }
        ?>
        <div class="partners__content">
          <h3 class="section__text partners__subtitle">Плюсы сотрудничества</h3>
          <p class="section__text partners__text"><?php the_field('partners__advantages-text'); ?></p>
          <h3 class="section__text partners__subtitle">Хотите стать нашим партнёром?</h3>          
           <?php echo do_shortcode('[contact-form-7 id="251" title="Хотите стать нашим партнёром?" html_class="partners__form"]') ?>
        </div>
      </div>
    </div>
  </div>
  <!-- about-partners-->
  <div class="about-partners">
    <div class="section__container about-partners__container">
      <h2 class="section__subtitle about-partners__title">Подобнее о наших партнёрах</h2>
      <?php
      if (have_rows('partners__list')) {
      ?>
      <ul class="about-partners__list">
        <?php
        while (have_rows('partners__list')) {
          the_row();
          ?>
          <li class="about-partners__item">
            <div class="about-partners__image"><img src="<?php the_sub_field('partners__logo'); ?>" alt=""></div>
            <div class="about-partners__content">
              <h3 class="section__text about-partners__subtitle"><?php the_sub_field('partners__name'); ?></h3>
              <p class="section__text section__text--small about-partners__text"><?php the_sub_field('partners__text'); ?></p>
            </div>
          </li>
        <?php
        }
        ?>
        <li class="about-partners__item">
          <div class="about-partners__image about-partners__image--offer"></div>
          <div class="about-partners__content">
            <h3 class="section__text about-partners__subtitle">Хотите стать нашим партнёром?</h3>
            <form class="partners__form">
              <input type="text" placeholder="Имя" class="partners__input">
              <input type="tel" placeholder="Телефон" class="partners__input">
              <input type="submit" value="Да, начать сотрудничать" class="btn partners__submit">
            </form>
          </div>
        </li>
      </ul>
      <?php
      }
      ?>
    </div>
  </div>
</main>
<?php get_footer(); ?>
