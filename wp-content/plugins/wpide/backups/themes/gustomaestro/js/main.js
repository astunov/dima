'use strict';

// Подставляет нужное склонение «Осталось 3 места»
var currentWord = function(billetLeftText, wordNameCase, wordParentCase, wordMultiple) {
  $(billetLeftText).each(function() {
    var billetLeftItem = $(this),
        billetLeftNumber = parseInt(billetLeftItem.text()),
        n;

    n = parseInt(billetLeftNumber / 10);

    if (n == 0) {
      n = '';
    }

    if (billetLeftNumber == (n + '1')) {
      billetLeftItem.append(' ' + wordNameCase);
    } else if (billetLeftNumber > (n + '1') && billetLeftNumber < (n + '5')) {
      billetLeftItem.append(' ' + wordParentCase);
    } else if (billetLeftNumber > (n + '4') && billetLeftNumber < (n + '21')) {
      billetLeftItem.append(' ' + wordMultiple);
    }
  });
}

// Состояние карточки: записаться в лист ожидания
var setWorkshopPreviewState = function(billetLeftClass) {
  var workshopBilletLeft = $(billetLeftClass);

  workshopBilletLeft.each(function() {
    var workshopImageWrapper = $(this).closest('.mc-preview__item').find('.mc-preview__image');

    if ($(this).hasClass('mc-preview--noplaces')) {
      workshopImageWrapper.find('a.mc-preview__btn').remove();
      workshopImageWrapper.append('<a href="#" data-toggle="modal" data-target="#modalWaitingList" class="btn mc-preview__btn mc-preview__btn--center">Записаться в лист ожидания</a>');
    } else if ($(this).hasClass('mc-preview--closed')) {
      var recipeFile = $(this).attr('data-recipe'),
          albumLink = $(this).attr('data-album');

      workshopImageWrapper.find('a.mc-preview__btn').remove();

      $(this).closest('.mc-preview__item').addClass('mc-preview__item--grayscale');

      workshopImageWrapper.append('<a href="' + recipeFile + '" target="_blank" class="btn mc-preview__btn mc-preview__btn--large">Скачать рецепт</a>');
      workshopImageWrapper.append('<a href="' + albumLink + '" target="_blank" class="btn mc-preview__btn mc-preview__btn--large mc-preview__btn--enroll">Смотреть фотоотчёт</a>');

    }
  });
}

// Подставляет день относительно формата «12 ноября 2015»
var getDayInStr = function(dateText) {
  $(dateText).each(function() {
    var currentWorkshopDate = $(this),
        workshopDateStr = currentWorkshopDate.text(),
        mounthInNumbers = new Array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');

    workshopDateStr = workshopDateStr.split(' ');

    for (var i = 0; i < mounthInNumbers.length; i++) {
      if (workshopDateStr[1] == mounthInNumbers[i]) {
        workshopDateStr[1] = i;
      }
    }

    var currentDate = new Date(workshopDateStr[2], workshopDateStr[1], workshopDateStr[0]),
        currentDay = currentDate.getDay(),
        daysInStr = new Array('Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб');

    for (var i = 0; i < daysInStr.length; i++) {
      if (currentDay == i) {
        currentDay = daysInStr[i];
      }
    }

    currentWorkshopDate.before(currentDay + ' ');
  });
}

$(document).ready(function() {

  // Открывает мобильное меню
  var openMobileMenu = function(btn, menu) {
    $(btn).on('click', function(e) {
      e.preventDefault();

      if ($(this).attr('data-open') == 'false') {
        $(this).attr('data-open', 'true').addClass('open');

        $(menu).slideDown(300);
      } else {
        $(this).attr('data-open', 'false').removeClass('open');

        $(menu).slideUp(300);
      }
    });
  }

  // Выравнивает изображения по центру контейнера
  var imageToCenter = function(imageContainer) {
    $(imageContainer).each(function() {
      var boxHeight = $(this).height(),
          boxWidth = $(this).width(),
          boxImage = $(this).find('img'),
          boxImageHeight = boxImage.height(),
          boxImageWidth = boxImage.width();

      if (boxImageHeight > boxImageWidth) {
        boxImage.addClass('full-width').removeClass('full-height');
      } else {
        // boxImage.addClass('full-height').removeClass('full-width');
        boxImage.addClass('full-width').removeClass('full-height');
      }

      var newBoxImageHeight = boxImage.height(),
          newBoxImageWidth = boxImage.width(),
          halfMarginHeight = (newBoxImageHeight - boxHeight) / 2,
          halfMarginWidth = (newBoxImageWidth - boxWidth) / 2;

      if (newBoxImageHeight < newBoxImageWidth) {
        boxImage.css({marginLeft: '-' + halfMarginWidth + 'px', marginTop: 0});//offset left
      } else {
        boxImage.css({marginTop: '-' + halfMarginHeight + 'px', marginLeft: 0});//offset top
      }
    });
  }

  // Определяет ориентацию картинки и задает ширину/высоту 100%
  var setImageSize = function(imageContainer) {
    $(imageContainer).each(function() {
      var boxImage = $(this).find('img'),
          boxImageHeight = boxImage.height(),
          boxImageWidth = boxImage.width();

      console.log(boxImage);
      console.log(boxImageHeight);
      console.log(boxImageWidth);

      if (boxImageHeight >= boxImageWidth) {
        boxImage.addClass('full-height');
      } else {
        boxImage.addClass('full-width');
      }
    });
  }

  // Открывает селектбокс в модальном окне, подставляет значения
  var openSelectbox = function(selectboxInModal) {
    $(selectboxInModal).on('click', function(e) {
      e.preventDefault();

      var selectbox = $(this),
          selectboxDropdown = $(this).closest('.selectbox__wrapper').find('.selectbox__dropdown'),
          selectboxItems = selectboxDropdown.find('.selectbox__item'),
          selectboxOpen = selectbox.attr('data-open'),
          selectboxResult = selectbox.find('.jsSelectboxResult');

      selectbox.addClass('disabled').attr('data-open', 'true');
      selectboxDropdown.addClass('open');

      selectboxItems.on('click', function() {
        var itemTitle = $(this).text(),
            itemDate = $(this).attr('data-date'),
            itemTime = $(this).attr('data-time'),
            itemPrice = $(this).attr('data-price');

        selectboxItems.each(function() {
          $(this).removeClass('active');
        });

        $(this).addClass('active');

        selectboxResult.text(itemTitle);

        $('#selectDate').text(itemDate);
        $('#selectTime').text('в ' + itemTime);
        $('#selectPrice').text(itemPrice);
      });

      e.stopPropagation();
      $(document).on('click', function() {
        selectbox.removeAttr('data-open').removeClass('disabled');
        selectboxDropdown.removeClass('open');
      });
    });
  }

  // Выствляет первое значение в селектбоксе при открытии модального окна
  var setFirstItemToSelectbox = function(modalId) {
    $('a[data-target="' + modalId + '"]').on('click', function() {
      var selectbox = $('.selectbox'),
          selectboxDropdown = selectbox.closest('.selectbox__wrapper').find('.selectbox__dropdown'),
          selectboxItem = selectboxDropdown.find('.selectbox__item:first-child'),
          selectboxResult = selectbox.find('.jsSelectboxResult'),
          selectDate =  $('#selectDate'),
          selectTime = $('#selectTime'),
          selectPrice = $('#selectPrice');

      selectboxResult.text(selectboxItem.text());
      selectDate.text(selectboxItem.attr('data-date'));
      selectTime.text(selectboxItem.attr('data-time'));
      selectPrice.text(selectboxItem.attr('data-price'));
    });
  }

  // Увеличивает число при кликах на кнопки
  var changeRubles = function(changeElement, plusBtn, minusBtn, step, minValue, resultPriceElement, currentPriceElement) {
    var customRublesResult = $(changeElement),
        customRublesPlus = $(plusBtn),
        customRublesMinus = $(minusBtn),
        resultPrice = $(resultPriceElement),
        currentPrice = $(currentPriceElement).text();

    // Считает сумму денег заказа
    var countSubRubles = function(currentSum) {
      currentPrice = parseInt(currentPrice);

      resultPrice.text(currentSum * currentPrice + ' руб.');
    }

    customRublesResult.on('change', function() {
      var billetLeft = $('#billetLeft'),
          ctrl = $(this).val();

      if (billetLeft.length !== 0) {
        billetLeft = parseInt(billetLeft.text());

        if (ctrl > billetLeft) {
          ctrl = billetLeft;
          $(this).val(billetLeft);
        }
      }

      if (resultPrice && currentPrice) {countSubRubles(ctrl);}
    });

    customRublesPlus.on('click', function() {
      var customRublesCurrent = parseInt(customRublesResult.text()),
          billetLeft = $('#billetLeft'),
          checkVal = false;

      if (isNaN(customRublesCurrent)) {
        customRublesCurrent = parseInt(customRublesResult.val());
        checkVal = true;
      }

      customRublesCurrent += step;

      if (billetLeft.length !== 0) {
        billetLeft = parseInt(billetLeft.text());

        if (customRublesCurrent <= billetLeft) {
          if (checkVal == false) {
            customRublesResult.text(customRublesCurrent);
          } else {
            customRublesResult.val(customRublesCurrent);
          }

          if (resultPrice && currentPrice) {countSubRubles(customRublesCurrent);}
        }
      } else {
        if (checkVal == false) {
          customRublesResult.text(customRublesCurrent);
        } else {
          customRublesResult.val(customRublesCurrent);
        }
      }
    });

    customRublesMinus.on('click', function() {
      var customRublesCurrent = parseInt(customRublesResult.text()),
          checkVal = false;

      if (isNaN(customRublesCurrent)) {
        customRublesCurrent = parseInt(customRublesResult.val());
        checkVal = true;
      }

      customRublesCurrent -= step;

      if (customRublesCurrent >= minValue) {
        if (checkVal == false) {
          customRublesResult.text(customRublesCurrent);
        } else {
          customRublesResult.val(customRublesCurrent);
        }

        if (resultPrice && currentPrice) {countSubRubles(customRublesCurrent);}
      }
    });
  }

  // Состояние карточки: записаться в лист ожидания
  setWorkshopPreviewState('.mc-preview__billet-left');

  // Задаем шаблон для полей с вводом телефона
  $('input[name="tel"]').mask('+7 (999) 999-99-99',{placeholder:'+7 (___) ___-__-__'});

  // Убираем ошибку инпута при фокусе
  $('input').not(':input[type=button], :input[type=submit], :input[type=reset]').on('focus', function() {
    var currentInput = $(this);

    if (currentInput.hasClass('error')) {
      $(this).removeClass('error');
    }
  });

  // Увеличиваем количество бабла при кликах на кнопки
  changeRubles('#customRublesResult', '#customRublesPlus', '#customRublesMinus', 500, 3000);

  // Селектбоксы в модальных окнах
  setFirstItemToSelectbox('#modalCurrentCertificate');
  openSelectbox('#modalSelectbox');

  // Следит за изменением размеров
  $(window).resize(function() {
    imageToCenter('.mc-preview__image');
    imageToCenter('.gallery__item');
    imageToCenter('.col-text-col-image__image');
    imageToCenter('.mc-promo__image');
    imageToCenter('.main-dishes__image');
    imageToCenter('.comments__image');
    imageToCenter('.certificates__promo-image');
    imageToCenter('.celebration-events__item');
    setImageSize('.about-partners__image');
  });

  // Список центрируемых изображений
  imageToCenter('.mc-preview__image');
  imageToCenter('.gallery__item');
  imageToCenter('.col-text-col-image__image');
  imageToCenter('.mc-promo__image');
  imageToCenter('.main-dishes__image');
  imageToCenter('.comments__image');
  imageToCenter('.certificates__promo-image');
  imageToCenter('.celebration-events__item');
  setImageSize('.about-partners__image');

  // Мобильное меню
  openMobileMenu('#openMenu', '#mobileMenu');

  // Подставляем правильные падежи «Осталось мест»
  currentWord('.jsCurrentWord', 'место', 'места', 'мест');
  currentWord('.commentsNumber', 'комментарий', 'комментария', 'комментариев');

  // Подставляем сокращенный текст дня в превьюшке мастер-класса
  getDayInStr('.jsWorkshopDate');

  $('#modalWorkshopOrder').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget),
        title = button.data('title'),
        date = button.data('date'),
        time = button.data('time'),
        cost = button.data('cost'),
        billetLeft = button.data('billet-left'),
        modal = $(this);

    modal.find('#modalTitle').text(title);
    modal.find('#modalDate').text(date);
    getDayInStr('#modalDate');
    modal.find('#modalTime').text('в ' + time);
    modal.find('#currentPrice').text(cost + ' руб.');
    modal.find('#billetLeft').text(billetLeft);
    currentWord('#billetLeft', 'место', 'места', 'мест');
    modal.find('#resultPrice').text(cost * modal.find('#personResult').val() + ' руб.');

    // Увеличиваем количество бабла при кликах на кнопки
    changeRubles('#personResult', '#plusPerson', '#minusPerson', 1, 1, '#resultPrice', '#currentPrice');
  });

  // Галерея
  $('.swipebox').swipebox();

});
