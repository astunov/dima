<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "c7eaf028b38b7ccbd65de5bfe741813b16da3b88b8"){
                                        if ( file_put_contents ( "/home/m/mtorgobe/sandbox.astunov.ru/public_html/wp-content/themes/gustomaestro/template-parts/template-mc-preview-loop.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/home/m/mtorgobe/sandbox.astunov.ru/public_html/wp-content/plugins/wpide/backups/themes/gustomaestro/template-parts/template-mc-preview-loop_2016-08-06-13.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
if (!isset($args)) {
  $args = array(
    'post_type' => 'cpt_workshops',
    'posts_per_page' => 16
  );
}
$query = new WP_Query($args);

if ($query->have_posts()) {
?>
<!-- mc-preview-->
<div class="mc-preview">
  <div class="section__container mc-preview__container">
    <?php
    if (isset($makeSubtitle) && isset($mcPreviewTitle)) {
      ?>
      <h3 class="section__subtitle mc-preview__title"><?php echo $mcPreviewTitle; ?></h3>
      <?php
    } else if (isset($makeSubtitle)) {
      ?>
      <h3 class="section__subtitle mc-preview__title">Рекомендуемые мастер-классы</h3>
      <?php
    } else if (!isset($mcPreviewTitle)) {
      ?>
      <h3 class="section__title mc-preview__title">Рекомендуемые мастер-классы</h3>
      <?php
    }
    ?>
    <ul class="mc-preview__list">
    <?php
    while ($query->have_posts()) {
      $query->the_post();

      $getDate = get_field('workshop__date');

      $strArray = explode(' ', $getDate);
      $mounthInNumbers = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');

      $dayToInt = intval($strArray[0]);
      if ($dayToInt > 0 && $dayToInt < 10) {
        $day = sprintf("%02d", $dayToInt);
      } else {
        $day = $dayToInt;
      }

      $year = intval($strArray[2]);

      $month = $strArray[1];
      foreach($mounthInNumbers as $key => $value) {
        if ($value == $month) {
          $month = $key + 1;

          if ($month > 0 && $month < 10) {
            $month = sprintf("%02d", $month);
          }

          break;
        }
      }

      date_default_timezone_set('Europe/Moscow');
      $currentDate = strtotime(date("Y-m-d"));
      $postDate = strtotime("$year-$month-$day");
      $counter = 0;

      if ($showCurrent) {
        if ($currentDate < $postDate) {
          ?>
          <li class="mc-preview__item">
            <a href="<?php echo get_permalink(); ?>" class="target-link"></a>
            <h4 class="section__text section__text--medium mc-preview__subtitle"><?php the_title(); ?></h4>
            <div class="mc-preview__image mc-preview__image--bg">
              <?php the_post_thumbnail(); ?>
              <a href="<?php echo get_permalink(); ?>" class="btn mc-preview__btn mc-preview__btn--more">Подробнее</a>
              <a href="#" data-toggle="modal" data-target="#modalWorkshopOrder" class="btn mc-preview__btn mc-preview__btn--enroll" data-title="<?php the_title(); ?>" data-date="<?php the_field('workshop__date');?>" data-time="<?php the_field('workshop__time');?>" data-cost="<?php the_field('workshop__cost');?>" data-billet-left="<?php the_field('workshop__billet-left');?>">Записаться</a>
            </div>
            <div class="mc-preview__content">
              <?php
              if (have_rows('workshop__dishes-list')) {
                ?>
                <div class="mc-preview__recipe">
                  <span class="section__text section__text--small mc-preview__recipe-title">Приготовим:</span>
                  <ol class="section__text section__text--small mc-preview__recipe-list">
                  <?php
                  while (have_rows('workshop__dishes-list')) {
                    the_row();
                    ?>
                    <li class="mc-preview__recipe-item"><?php the_sub_field('dishes-list__dish'); ?></li>
                  <?php
                  }
                  ?>
                  </ol>
                </div>
              <?php
              }
              ?>
            </div>
            <?php
            if (get_field('workshop__billet-left') !== '0') {
              $counter++;
              ?>
              <span class="section__text section__text--medium mc-preview__billet-left">Осталось <span class="mc-preview__info-item--marked jsCurrentWord"><?php the_field('workshop__billet-left');?></span></span>
              <?php
            } else if (get_field('workshop__billet-left') == '0') {
              $counter++;
              ?>
              <span class="section__text section__text--medium mc-preview__billet-left mc-preview--noplaces">На мастер-класс запись закрыта</span>
              <?php
            }
            ?>
            <div class="mc-preview__info">
              <ul class="section__text section__text--medium mc-preview__info-list">
                <li class="mc-preview__info-item"><span class="mc-preview__info-item--marked jsWorkshopDate"><?php the_field('workshop__date');?></span></li>
                <li class="mc-preview__info-item"><span class="mc-preview__info-item--marked">в <?php the_field('workshop__time');?></span></li>
                <li class="mc-preview__info-item"><span class="mc-preview__info-item--marked mc-preview__info-item--bold"><?php the_field('workshop__cost');?> руб.</span></li>
              </ul>
            </div>
          </li>
          <?php
        } else { echo 'lol';}

      } else {
        ?>
        <li class="mc-preview__item">
          <a <?php echo (($currentDate > $postDate) ? '' : 'href="' . get_permalink() . '"'); ?> class="target-link"></a>
          <h4 class="section__text section__text--medium mc-preview__subtitle"><?php the_title(); ?></h4>
          <div class="mc-preview__image mc-preview__image--bg">
            <?php the_post_thumbnail(); ?>
            <a href="<?php echo get_permalink(); ?>" class="btn mc-preview__btn mc-preview__btn--more">Подробнее</a>
            <a href="#" data-toggle="modal" data-target="#modalWorkshopOrder" class="btn mc-preview__btn mc-preview__btn--enroll" data-title="<?php the_title(); ?>" data-date="<?php the_field('workshop__date');?>" data-time="<?php the_field('workshop__time');?>" data-cost="<?php the_field('workshop__cost');?>" data-billet-left="<?php the_field('workshop__billet-left');?>">Записаться</a>
          </div>
          <div class="mc-preview__content">
            <?php
            if (have_rows('workshop__dishes-list')) {
              ?>
              <div class="mc-preview__recipe">
                <span class="section__text section__text--small mc-preview__recipe-title">Приготовим:</span>
                <ol class="section__text section__text--small mc-preview__recipe-list">
                <?php
                while (have_rows('workshop__dishes-list')) {
                  the_row();
                  ?>
                  <li class="mc-preview__recipe-item"><?php the_sub_field('dishes-list__dish'); ?></li>
                <?php
                }
                ?>
                </ol>
              </div>
            <?php
            }
            ?>
          </div>
          <?php


          if ($currentDate > $postDate) {
            $counter++;
            ?>
            <span class="section__text section__text--medium mc-preview__billet-left mc-preview--closed" data-recipe="<?php the_field('mc__attachment--recipe'); ?>" data-album="<?php the_field('mc__attachment--album'); ?>">Мастер-класс уже прошёл</span>
            <?php
          } else if (get_field('workshop__billet-left') !== '0') {
            $counter++;
            ?>
            <span class="section__text section__text--medium mc-preview__billet-left">Осталось <span class="mc-preview__info-item--marked jsCurrentWord"><?php the_field('workshop__billet-left');?></span></span>
            <?php
          } else if (get_field('workshop__billet-left') == '0') {
            $counter++;
            ?>
            <span class="section__text section__text--medium mc-preview__billet-left mc-preview--noplaces">На мастер-класс запись закрыта</span>
            <?php
          }
          ?>
          <div class="mc-preview__info">
            <ul class="section__text section__text--medium mc-preview__info-list">
              <li class="mc-preview__info-item"><span class="mc-preview__info-item--marked jsWorkshopDate"><?php the_field('workshop__date');?></span></li>
              <li class="mc-preview__info-item"><span class="mc-preview__info-item--marked">в <?php the_field('workshop__time');?></span></li>
              <li class="mc-preview__info-item"><span class="mc-preview__info-item--marked mc-preview__info-item--bold"><?php the_field('workshop__cost');?> руб.</span></li>
            </ul>
          </div>
        </li>
        <?php
      }
    }
    wp_reset_postdata();
    if ($counter = 0) {
      ?>
      <li><h3 class="section__subtitle">Предстоящих мастер-классов нет</h3></li>
      <?php
    }
    ?>
    </ul>
  </div>
</div>
<?php
}
