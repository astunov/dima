<?php get_header(); ?>
<?php include(locate_template('template-parts/template-breadcrumbs.php')) ?>
<main class="main">
  <div class="section__container main__container">
    <!-- article-->
    <article class="article">
      <?php the_post(); ?>
      <div class="article__image"><?php the_post_thumbnail(); ?></div>
      <h3 class="section__subtitle article__title"><?php the_title(); ?></h3>
      <div class="article__info">
        <span class="section__text article__text--serif "><?php the_time('d F Y'); ?></span>
        <span class="section__text article__text--serif commentsNumber"><?php if(get_comments_number() == 0): echo 'Нет комментариев'; else: echo get_comments_number(); endif; ?></span>
      </div>
      <div class="article__content">
        <?php
        function first_paragraph($content) {
          return preg_replace('/<p([^>]+)?>/', '<p$1 class="section__text article__text">', $content);
        }
        add_filter('the_content', 'first_paragraph');

        the_content();
        ?>
      </div>
      <!-- share-->
      <div class="share"><span class="section__text share__title">Поделиться:</span>
        <ul class="share__list">
          <li class="share__item"><a href="#" class="share__item--facebook">Facebook</a></li>
          <li class="share__item"><a href="#" class="share__item--twitter">Twitter</a></li>
          <li class="share__item"><a href="#" class="share__item--mail">Мой мир</a></li>
          <li class="share__item"><a href="#" class="share__item--vk">Вконтакте</a></li>
          <li class="share__item"><a href="#" class="share__item--google">Google+</a></li>
        </ul>
      </div>
      <!-- other-posts-->
      <div class="other-posts">
        <h2 class="section__subtitle other-posts__title">Советуем прочесть</h2>
        <ul class="other-posts__list">
          <li class="other-posts__item"><a href="#">
              <div class="other-posts__image"><img src="images/article__promo--preview1.jpg" alt=""></div><a href="#" class="link other-posts__link">Кажется, будто это было вчера, а прошёл уже целый год!</a></a></li>
          <li class="other-posts__item"><a href="#">
              <div class="other-posts__image"><img src="images/article__promo--preview2.jpg" alt=""></div><a href="#" class="link other-posts__link">Россия не для всех!</a></a></li>
        </ul>
      </div>
      <!-- comments-->
      <div class="comments">
        <h2 class="section__subtitle comments__title">Комментарии | 3</h2>
        <ul class="comments__list">
          <li class="comments__item">
            <div class="comments__image"><img src="images/comment-author__image1.jpg" alt=""></div>
            <div class="comments__content">
              <div class="comments__info">
                <ul class="section__text comments__info-list">
                  <li class="comments__info-item">Sofia</li>
                  <li class="comments__info-item">14 ноября 2014</li>
                  <li class="comments__info-item">13:43</li>
                </ul>
              </div>
              <p class="section__text section__text--small comments__text">The review is good as aalyws telling about the theme and at the same time not telling the story eventually creating interest to watch the movie. But one thing to correct - cast in Dil chahta hai is "Akshay khanna" not Rahul apart from Aamir & saif -Purnima</p>
            </div>
          </li>
          <li class="comments__item">
            <div class="comments__image"><img src="images/comment-author__image2.jpg" alt=""></div>
            <div class="comments__content">
              <div class="comments__info">
                <ul class="section__text comments__info-list">
                  <li class="comments__info-item">Udin</li>
                  <li class="comments__info-item">17 ноября 2014</li>
                  <li class="comments__info-item">21:58</li>
                </ul>
              </div>
              <p class="section__text section__text--small comments__text">Dear friend,I saw the blog. It is very nice, can u pl tell me what is the comirecmal benefit of running this?mail your reply when free, and if you like to reply. my mail id is callmesrk@gmail.com.RegardsRavi.S http://xhzfwrluji.com [url=http://ibabtw.com]ibabtw[/url] [link=http://lscklxamj.com]lscklxamj[/link]</p>
            </div>
          </li>
          <li class="comments__item">
            <div class="comments__image"><img src="images/comment-author__image3.jpg" alt=""></div>
            <div class="comments__content">
              <div class="comments__info">
                <ul class="section__text comments__info-list">
                  <li class="comments__info-item">Kim</li>
                  <li class="comments__info-item">19 ноября 2014</li>
                  <li class="comments__info-item">19:19</li>
                </ul>
              </div>
              <p class="section__text section__text--small comments__text">Dear friend,I saw the blog. It is very nice, can u pl tell me what is the comirecmal benefit of running this?mail your reply when free, and if you like to reply. my mail id is callmesrk@gmail.com.RegardsRavi.S http://xhzfwrluji.com [url=http://ibabtw.com]ibabtw[/url] [link=http://lscklxamj.com]lscklxamj[/link]</p>
            </div>
          </li>
        </ul>
        <div class="comments__write">
          <h2 class="section__subtitle comments__write-title">Оставьте свой комментарий</h2>
          <form class="comments__form">
            <div class="comments__inputs-wrapper">
              <div class="comments__inputs">
                <input type="text" placeholder="Имя" class="comments__input">
                <input type="tel" placeholder="Телефон" class="comments__input">
                <input type="email" placeholder="E-mail" class="comments__input">
              </div>
              <div class="comments__inputs comments__inputs--large">
                <textarea placeholder="Комментарий" class="comments__textarea"></textarea>
              </div>
            </div>
            <div class="comments__submits"><a href="#" class="section__text link comments__attach">Загрузить свою фотографию</a>
              <input type="submit" value="Отправить комментарий" class="btn comments__submit">
            </div>
          </form>
        </div>
      </div>
    </article>
    <!-- aside-->
    <aside class="aside">
      <div class="aside__about-chief">
        <h3 class="section__subtitle aside__title">Фабрицио Фаттучи</h3>
        <p class="section__text aside__text">Приходя в «Gusto», Вы попадаете в гости к знаменитому шеф-повару из Италии Фабрицио Фатуччи. Искусство готовить, по версии мэтра, – это, прежде всего, чувства, эмоции и настроение, которые можно подарить другим. В поисках этих главных ингредиентов, уроженец Рима, он объездил всю Италию. Впитал ароматы Тосканы, Сицилии, Неаполя, и наполнил ими свои блюда. Его кредо - импровизация с душой. Отточив мастерство в десятках лучших заведений, от лондонского гранд-отеля «Savoy» до московского «Forbes», в Петербурге Фабрицио осуществил свою мечту – о «живом» и «тёплом» ресторане, где гости чувствуют себя как дома [...]</p><a href="#" class="section__text link aside__link">Подробнее о Фабрицио</a>
      </div>
      <div class="aside__popular">
        <h3 class="section__subtitle aside__title">Чаще всего читают</h3>
        <ul class="aside__list section__text">
          <li class="aside__item"><a href="#" class="link">Россия не для всех</a></li>
          <li class="aside__item"><a href="#" class="link">Кажется, будто это было вчера, а прошёл уже целый год!</a></li>
          <li class="aside__item"><a href="#" class="link">Фабрицио Фатуччи об эстетике формы и цвета</a></li>
          <li class="aside__item"><a href="#" class="link">«Яичница-болтунья»</a></li>
          <li class="aside__item"><a href="#" class="link">В гостях у Пятого канала</a></li>
        </ul>
      </div>
      <div class="aside__subscribe">
        <p class="aside__subscribe-text">Подписка на группу вконтакте</p>
      </div>
      <div class="aside__subscribe">
        <p class="aside__subscribe-text">Подписка Facebook</p>
      </div>
    </aside>
  </div>
</main>
<?php get_footer(); ?>
