<?php get_header(); ?>
<main class="main">
  <!-- promo-->
  <section class="promo promo--main">
    <div class="section__container promo__container">
      <div class="promo__offer"><i class="spoon-fork"></i>
        <h1 class="section__header promo__title"><?php the_field('front-page-promo__title'); ?></h1>
        <span class="section__subtitle promo__subtitle"><?php the_field('front-page-promo__subtitle'); ?></span>
      </div>
    </div>
    <div class="promo__video">
      <video autoplay poster="<?php bloginfo('template_directory'); ?>/images/promo--home.jpg" loop>
        <source src="<?php bloginfo('template_directory'); ?>/images/video-bg.webm" type="video/webm">
        <source src="<?php bloginfo('template_directory'); ?>/images/video-bg.mp4" type="video/mp4">
      </video>
    </div>
  </section>

  <?php
  $args = array(
    'post_type' => 'cpt_workshops',
    'liked_workshops' => 'home',
    'posts_per_page' => 4,
    'orderby' => "date"
  );
  include(locate_template('template-parts/template-mc-preview-loop.php'));
  ?>

  <div class="calendar" id="calendar">
    <div class="section__container calendar__container">
      <h3 class="section__title calendar__title">Расписание мастер-классов и частных мероприятий</h3>
      <div class="cal1"></div>
    </div>
  </div>

  <?php
  $advantagesTitleBig = true;
  include(locate_template('template-parts/template-advantages.php')); ?>

  <?php
  $postID = 81; // Превью «На мастер классах»
  $permalinkPageName = 'workshops';
  $btnTitle = 'Посмотреть расписание';
  include(locate_template('template-parts/template-image-text--right.php'));
  ?>

  <?php
  $postID = 82; // Превью «На праздниках и корпоративах»
  $permalinkPageName = 'celebration';
  $btnTitle = 'Узнать больше и заказать';
  include(locate_template('template-parts/template-image-text--left.php'));
  ?>

  <?php
  $postID = 83; // Превью «Подарочный сертификат»
  $permalinkPageName = 'certificates';
  $btnTitle = 'Выбрать сертификат';
  include(locate_template('template-parts/template-image-text--right.php'));
  ?>

  <?php include(locate_template('template-parts/template-contacts.php')); ?>
</main>
<?php include(locate_template('template-parts/template-modal-order.php')); ?>
<?php include(locate_template('template-parts/template-modal-waiting-list.php')); ?>

<?php echo((is_front_page()) ? '<style>.mc-preview__item:nth-of-type(1) {display:none;} @media (max-width: 1207px){.mc-preview__item:nth-of-type(1){display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;}}</style>' : false) ?>

<?php get_footer(); ?>
