<?php get_header(); ?>
<!-- promo-->
<section class="promo promo--blog" style="background: url(<?php the_field('promo__background'); ?>) no-repeat center/cover;">
  <div class="section__container promo__container">
    <h1 class="section__header promo__title"><?php the_field('blog__title'); ?></h1>
  </div>
</section>
<main class="main main--articles">
  <div class="section__container main__container">
    <!-- articles-->
    <section class="articles">
      <?php
      if (have_posts()) {
        ?>
        <ul class="articles__list">
        <?php
        while (have_posts()) {
          the_post();
          ?>
          <li class="articles__item">
            <h3 class="section__subtitle articles__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <div class="articles__info">
              <span class="section__text articles__text--serif"><?php the_time('d F Y'); ?></span>
              <span class="section__text articles__text--serif commentsNumber"><?php if(get_comments_number() == 0): echo 'Нет комментариев'; else: echo get_comments_number(); endif; ?></span>
            </div>
            <div class="articles__image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('normal'); ?></a></div>
            <p class="section__text articles__text"><?php echo get_the_excerpt(); ?></p>
            <a href="<?php the_permalink(); ?>" class="section__text link">Читать далее</a>
          </li>
          <?php
        }
        the_posts_navigation();
        ?>
        </ul>
        <?php
      } else {
        get_template_part( 'template-parts/content', 'none' );
      }
      ?>
      <!-- pagination-->
      <!-- <nav class="pagination">
        <ul class="pagination__list">
          <li class="pagination__item"><a href="#" class="pagination__arrow"></a></li>
          <li class="pagination__item"><a href="#">1</a></li>
          <li class="pagination__item"><a href="#" class="active">2</a></li>
          <li class="pagination__item"><a href="#">3</a></li>
          <li class="pagination__item">...</li>
          <li class="pagination__item"><a href="#">12</a></li>
          <li class="pagination__item"><a href="#" class="pagination__arrow pagination__arrow--left"></a></li>
        </ul>
      </nav> -->
    </section>
    <!-- aside-->
    <aside class="aside">
      <?php get_sidebar(); ?>
      <div class="aside__about-chief">
        <h3 class="section__subtitle aside__title">Фабрицио Фаттучи</h3>
        <p class="section__text aside__text">Приходя в «Gusto», Вы попадаете в гости к знаменитому шеф-повару из Италии Фабрицио Фатуччи. Искусство готовить, по версии мэтра, – это, прежде всего, чувства, эмоции и настроение, которые можно подарить другим. В поисках этих главных ингредиентов, уроженец Рима, он объездил всю Италию. Впитал ароматы Тосканы, Сицилии, Неаполя, и наполнил ими свои блюда. Его кредо - импровизация с душой. Отточив мастерство в десятках лучших заведений, от лондонского гранд-отеля «Savoy» до московского «Forbes», в Петербурге Фабрицио осуществил свою мечту – о «живом» и «тёплом» ресторане, где гости чувствуют себя как дома [...]</p><a href="chief.html" class="section__text link aside__link">Подробнее о Фабрицио</a>
      </div>
      <div class="aside__popular">
        <h3 class="section__subtitle aside__title">Чаще всего читают</h3>
        <ul class="aside__list section__text">
          <li class="aside__item"><a href="article.html" class="link">Россия не для всех</a></li>
          <li class="aside__item"><a href="article.html" class="link">Кажется, будто это было вчера, а прошёл уже целый год!</a></li>
          <li class="aside__item"><a href="article.html" class="link">Фабрицио Фатуччи об эстетике формы и цвета</a></li>
          <li class="aside__item"><a href="article.html" class="link">«Яичница-болтунья»</a></li>
          <li class="aside__item"><a href="article.html" class="link">В гостях у Пятого канала</a></li>
        </ul>
      </div>
      <div class="aside__subscribe">
        <p class="aside__subscribe-text">Подписка на группу вконтакте</p>
      </div>
      <div class="aside__subscribe">
        <p class="aside__subscribe-text">Подписка Facebook</p>
      </div>
    </aside>
  </div>
</main>
<?php get_footer(); ?>
