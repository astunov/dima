jQuery(document).ready(function ($) {

  var workshopsTab = function(switchBtnId) {
    $(switchBtnId).on('click', function(e) {
      e.preventDefault();

      $('.mc-preview').prepend('<div class="mc-background"></div>');
      $('.mc-preview').prepend('<div id="circularG"><div id="circularG_1" class="circularG"></div><div id="circularG_2" class="circularG"></div><div id="circularG_3" class="circularG"></div><div id="circularG_4" class="circularG"></div><div id="circularG_5" class="circularG"></div><div id="circularG_6" class="circularG"></div><div id="circularG_7" class="circularG"></div><div id="circularG_8" class="circularG"></div></div>');
      var mcBackground = $('.mc-background'),
          mcLoader = $('#circularG'),
          ctrl = $(this),
          dataTax = ctrl.attr('data-tax'),
          selectbox = $('#modalSelectbox');
          selectboxResult = selectbox.find('.jsSelectboxResult'),
          selectboxItems = $(document).find('.selectbox__item'),
          currentDate = '';

      if (dataTax == undefined) {
        dataTax = '';
      }

      selectboxItems.each(function() {
        if (selectboxResult.text() == $(this).find('.selectbox__text').text()) {
          currentDate = $(this).attr('data-workshop-month');
        }
      });

      if (currentDate == undefined) {
        currentDate = '';
      }

      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: workshops_object.ajaxurl,
        data: {
          'action': 'setlistbycheckbox',
          'tax': dataTax,
          'currentDate': currentDate,
        },
        beforeSend: function() {
          mcBackground.css('opacity', 1);
          mcLoader.css('display', 'block');
        },
        success: function(data) {
          $('.mc-preview__list').remove();
          mcBackground.fadeOut(200);
          mcLoader.fadeOut(200);

          ctrl.closest('.workshops__tab-radio-list').find('a').removeClass('active');
          ctrl.addClass('active');
          $('.mc-preview__container').prepend(data.worksoplist)
          currentWord('.workshops .jsCurrentWord', 'место', 'места', 'мест');
          getDayInStr('.workshops .jsWorkshopDate');
          setWorkshopPreviewState('.mc-preview__billet-left');
        },
        error: function(data) {
          mcBackground.fadeOut(200);
          mcLoader.fadeOut(200);
        }
      });
    });
  }

  var selectboxInit = function(callback) {
    var selectbox = $('#modalSelectbox');

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: workshops_object.ajaxurl,
      data: {
        'action': 'checkboxslist',
      },
      beforeSend: function() {
        selectbox.addClass('disabled');
      },
      success: function(data) {
        selectbox.removeClass('disabled');
        $('.selectbox__dropdown').prepend(data.selectboxItems);
        var selectboxItems = $(document).find('.selectbox__item'),
            selectboxItemsDates = new Array(),
            i = 0,
            mounthInNumbers = new Array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');

        selectboxItems.each(function() {
          if ($(this).find('.selectbox__text').text() == 'Предстоящие') {
            $(this).addClass('active')
          } else {
            selectboxItemsDates[i] = $(this).attr('data-workshop-month');
            var str = selectboxItemsDates[i].toString().split('-');

            if (str[1].charAt(0) === '0') {
              str[1] = str[1].substr(1);
            }

            for (var k = 0; k < mounthInNumbers.length; k++) {
              if (str[1] == k) {
                str[1] = mounthInNumbers[k - 1];
                break;
              }
            }

            $(this).find('.selectbox__text').text(str[1] + ' ' + str[0]);
          }

          i++;
        });

        if (callback !== undefined) {
          callback();
        }
      }
    });
  }

  if ($('.workshops').length) {
    selectboxInit(function() {

      var itemsToClick = $(document).find('.selectbox__item');

      itemsToClick.on('click', function(e) {
        e.preventDefault();

        $('.workshops').prepend('<div class="mc-background"></div>');
        $('.workshops').prepend('<div id="circularG"><div id="circularG_1" class="circularG"></div><div id="circularG_2" class="circularG"></div><div id="circularG_3" class="circularG"></div><div id="circularG_4" class="circularG"></div><div id="circularG_5" class="circularG"></div><div id="circularG_6" class="circularG"></div><div id="circularG_7" class="circularG"></div><div id="circularG_8" class="circularG"></div></div>');

        var itemsToClickDate = $(this).attr('data-workshop-month'),
            itemsWithTax = $('.workshops__tab-radio-list a'),
            itemTax,
            mcBackground = $('.mc-background'),
            mcLoader = $('#circularG');

        itemsWithTax.each(function() {
          if ($(this).hasClass('active')) {
            itemTax = $(this).attr('data-tax');
          }
        });

        if (itemTax == undefined) {
          itemTax = '';
        }

        if (itemsToClickDate == undefined) {
          itemsToClickDate = '';
        }

        $.ajax({
          type: 'POST',
          dataType: 'json',
          url: workshops_object.ajaxurl,
          data: {
            'action': 'setlistbycheckbox',
            'currentDate': itemsToClickDate,
            'tax': itemTax
          },
          beforeSend: function() {
            $('#modalSelectbox').addClass('disabled');
            mcBackground.css('opacity', 1);
            mcLoader.css('display', 'block');
          },
          success: function(data) {
            $('#modalSelectbox').removeClass('disabled');
            $('.mc-preview__list').remove();
            mcBackground.fadeOut(200);
            mcLoader.fadeOut(200);

            $('.mc-preview__container').prepend(data.worksoplist);
            currentWord('.workshops .jsCurrentWord', 'место', 'места', 'мест');
            getDayInStr('.workshops .jsWorkshopDate');
            setWorkshopPreviewState('.mc-preview__billet-left');
          },
          error: function(data) {
            mcBackground.fadeOut(200);
            mcLoader.fadeOut(200);
          }
        });
      });
    });
  }

  workshopsTab('.workshops__tab-radio-list a');
});
