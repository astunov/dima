jQuery(document).ready(function ($) {
  // Емейл-валидация
  function validateEmail(email) {
    var re = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
    return re.test(email);
  }

  // Проверка инпутов на пустоту и правильность емейла
  function validateInputs(formId, ignoreInputs) {
    var checkValid = true,
        formInputs = formId.find('input').not(':input[type=button], :input[type=submit], :input[type=reset]' + ignoreInputs);

    formInputs.each(function() {
      if ($(this).hasClass('error') || $(this).val() == '') {
        if (!$(this).hasClass('error')) {
          $(this).addClass('error');
        }

        checkValid = false;
      } else if ($(this).attr('name') == 'email') {
        if (!validateEmail($(this).val())) {
          $(this).addClass('error');

          checkValid = false;
        }
      }
    });

    return checkValid;
  }

  // Отправка письма
  var sendMail = function(formId) {
    $(formId).on('submit', function(e) {
      e.preventDefault();

      var currentForm = $(this),
          validator = validateInputs(currentForm);

      if (validator == false) {
        return false;
      }

      if (formId == '#formOrder') {
        var dataForm = {
          'action': 'ajaxorderworkshop',
          'nameVal': currentForm.find('input[name=name]').val(),
          'telVal': currentForm.find('input[name=tel]').val(),
          'emailVal': currentForm.find('input[name=email]').val()
        };
      } else if (formId == '#formOrderPreviewEvent') {
        var dataForm = {
          'action': 'ajaxorderpreviewevent',
          'nameVal': currentForm.find('input[name=name]').val(),
          'telVal': currentForm.find('input[name=tel]').val(),
          'commentVal': currentForm.find('input[name=comment]').val()
        };
      }

      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: ajax_auth_object.ajaxurl,
        data: dataForm,
        beforeSend: function() {
          currentForm.find('input[type="submit"]').attr('disabled', 'disabled');
        },
        success: function(data) {
          currentForm.find('input[type="submit"]').removeAttr('disabled');
          console.log(data.message);
        }
      });
    });
  }

  // Отправка письма
  sendMail('#formOrder');
  sendMail('#formOrderPreviewEvent');
});
