var calendars = {};

jQuery(document).ready(function ($) {
  // Assuming you've got the appropriate language files,
  // clndr will respect whatever moment's language is set to.
  // moment.locale('ru');

  // Here's some magic to make sure the dates are happening this month.
  var thisMonth = moment().format('YYYY-MM');
  // Events to load into calendar
  var eventArray = [
    {
      title: 'Multi-Day Event',
      endDate: thisMonth + '-14',
      startDate: thisMonth + '-10'
    }, {
      endDate: thisMonth + '-23',
      startDate: thisMonth + '-21',
      title: 'Another Multi-Day Event'
    }, {
      date: thisMonth + '-27',
      title: 'Single Day Event'
    }
  ];

  var setCurrentDates = function(callback) {
    $('.clndr').prepend('<div class="mc-background"></div>');
    $('.clndr').prepend('<div id="circularG"><div id="circularG_1" class="circularG"></div><div id="circularG_2" class="circularG"></div><div id="circularG_3" class="circularG"></div><div id="circularG_4" class="circularG"></div><div id="circularG_5" class="circularG"></div><div id="circularG_6" class="circularG"></div><div id="circularG_7" class="circularG"></div><div id="circularG_8" class="circularG"></div></div>');
    var mcBackground = $('.mc-background'),
        mcLoader = $('#circularG');

    var monthsEn = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        monthsRu = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        currentMonth = $('#calendarMonthTitle');

    for (var i = 0; i < monthsEn.length; i++) {
      if (monthsEn[i] == currentMonth.text()) {
        currentMonth.text(monthsRu[i]);
      }
    }

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: calendar_custom_object.ajaxurl,
      data: {
        'action': 'ajaxgetfield',
      },
      beforeSend: function() {
        mcBackground.css('opacity', 1);
        mcLoader.css('display', 'block');
      },
      success: function(data) {
        var workshopsDates = data.dates,
            datesInBase = new Array(),
            workshopIDs = data.IDs,
            tableDatesArray = $('td[class*="calendar-day"]').not('td[class*="last-month"]'),
            tableDates = new Array(),
            l = 0;

        for (var i = 0; i < workshopsDates.length; i++) {
          datesInBase[i] = workshopsDates[i].substr(0, 7);
        }

        var currentDate = $('td[class*="calendar-day"]').not('td[class*="last-month"]')[1],
            checkNextDate = true;
        currentDate = currentDate.getAttribute('class');
        var indexOfDate = currentDate.indexOf('calendar-day-');
        currentDate = currentDate.substr(indexOfDate + 13, 7);

        for (var i = 0; i < datesInBase.length; i++) {
          if (datesInBase[i] > currentDate) {
            checkNextDate = true;
            break;
          } else {
            checkNextDate = false;
          }
        }

        if (checkNextDate == false) {
          $('.clndr-next-button').addClass('disable');
        } else {
          $('.clndr-next-button').removeClass('disable');
        }

        tableDatesArray.each(function() {
          var tableDatesClass = $(this).attr('class'),
              indexOfClass = tableDatesClass.indexOf('calendar-day-');

          tableDates[l] = tableDatesClass.substr(indexOfClass + 13, 10);
          l++;
        });

        var checkin = false;

        for (var i = 0; i < workshopsDates.length; i++) {
          for (var k = 0; k < tableDates.length; k++) {
            if (workshopsDates[i] == tableDates[k]) {
              checkin = true;

              $.ajax({
                type: 'POST',
                dataType: 'json',
                url: calendar_custom_object.ajaxurl,
                data: {
                  'action': 'ajaxgetworkshop',
                  'ID': workshopIDs[i],
                  'date': workshopsDates[i],
                },
                success: function(data) {
                  var currentClass = $('td[class*=calendar-day-'+ data.date +']');

                  currentClass.append(data.postTitle);

                  if (data.active !== undefined) {
                    currentClass.closest('.day').find('.day-contents').addClass('active');
                  }

                  if (data.modalOutput !== undefined) {
                    currentClass.append(data.modalOutput);

                    var wordToChangeWrapper = currentClass.find('.jsCurrentWord');
                    currentWord(wordToChangeWrapper, 'место', 'места', 'мест');

                    // Состояние карточки: записаться в лист ожидания
                    setWorkshopPreviewState('.mc-preview__billet-left');

                    if (callback !== undefined) {
                      callback();
                    }
                  }

                  mcLoader.fadeOut(200);
                  mcBackground.fadeOut(200);
                },
                error: function() {
                  mcLoader.fadeOut(200);
                  mcBackground.fadeOut(200);
                }
              });
            }
          }
        }

        if (!checkin) {
          mcLoader.fadeOut(200);
          mcBackground.fadeOut(200);
        }
      },
      error: function() {
        mcLoader.fadeOut(200);
        mcBackground.fadeOut(200);
      }
    });
  }

  calendars.clndr1 = $('.cal1').clndr({
    events: eventArray,
    clickEvents: {
      onMonthChange: function () {
        setCurrentDates(function setmodals() {
          var workshopsPreview = $(document).find('.calendar-workshop-preview');

          workshopsPreview.on('mouseover', function() {
            if ($(this).hasClass('unactive')) {return false;}

            var preview = $(this),
                previewParent = preview.closest('.day'),
                previewParentForHeight = preview.closest('.calendar-row'),
                previewParentPosLeft = previewParent.position().left,
                previewParentPosTop = previewParentForHeight.position().top,
                previewParentWidth = parseInt(previewParent.css('width')),
                leftLimit = previewParentWidth * 4,
                workshopModal = preview.next(),
                workshopModalWitdh = parseInt(workshopModal.css('width')),
                workshopModalHeight = parseInt(workshopModal.css('height'));

            if (previewParentPosLeft > leftLimit && previewParentPosTop > workshopModalHeight) {
              workshopModal.css({
                'right': previewParentWidth - 25 + 'px',
                'bottom': 0,
                'display': 'block'
              });
            } else if (previewParentPosLeft > leftLimit && previewParentPosTop < workshopModalHeight) {
              workshopModal.css({
                'right': previewParentWidth - 25 + 'px',
                'top': 0,
                'display': 'block'
              });
            } else if (previewParentPosLeft < leftLimit && previewParentPosTop > workshopModalHeight) {
              workshopModal.css({
                'left': previewParentWidth - 25 + 'px',
                'bottom': 0,
                'display': 'block'
              });
            } else if (previewParentPosLeft < leftLimit && previewParentPosTop < workshopModalHeight) {
              workshopModal.css({
                'left': previewParentWidth - 25 + 'px',
                'top': 0,
                'display': 'block'
              });
            }

            workshopModal.on('mouseleave', function(e) {
              var previewOffset = preview.offset(),
                  previewWitdh = parseInt(preview.css('width')),
                  previewHeight = parseInt(preview.css('height'));
                  relX = e.pageX,
                  relY = e.pageY,
                  previewXLimit = previewOffset.left + previewWitdh,
                  previewYLimit = previewOffset.top + previewHeight;

              if (!(relX >= previewOffset.left && relX <= previewXLimit && relY >= previewOffset.top && relY <= previewYLimit)) {
                $(this).css('display', 'none');
              }
            });

            preview.on('mouseleave', function(e) {
              var modalOffset = workshopModal.offset(),
                  relX = e.pageX,
                  relY = e.pageY,
                  modalXLimit = modalOffset.left + workshopModalWitdh,
                  modalYLimit = modalOffset.top + workshopModalHeight;

              if (!(relX >= modalOffset.left && relX <= modalXLimit && relY >= modalOffset.top && relY <= modalYLimit)) {
                workshopModal.css('display', 'none');
              }

            });
          });
        });
      },
    },
    multiDayEvents: {
      singleDay: 'date',
      endDate: 'endDate',
      startDate: 'startDate'
    },
    showAdjacentMonths: true,
    adjacentDaysChangeMonth: false
  });

  if($('#calendar').length) {
    setCurrentDates(function setmodals() {
      var workshopsPreview = $(document).find('.calendar-workshop-preview');

      workshopsPreview.on('mouseover', function() {
        if ($(this).hasClass('unactive')) {return false;}

        var preview = $(this),
            previewParent = preview.closest('.day'),
            previewParentForHeight = preview.closest('.calendar-row'),
            previewParentPosLeft = previewParent.position().left,
            previewParentPosTop = previewParentForHeight.position().top,
            previewParentWidth = parseInt(previewParent.css('width')),
            leftLimit = previewParentWidth * 4,
            workshopModal = preview.next(),
            workshopModalWitdh = parseInt(workshopModal.css('width')),
            workshopModalHeight = parseInt(workshopModal.css('height'));

        if (previewParentPosLeft > leftLimit && previewParentPosTop > workshopModalHeight) {
          workshopModal.css({
            'right': previewParentWidth - 25 + 'px',
            'bottom': 0,
            'display': 'block'
          });
        } else if (previewParentPosLeft > leftLimit && previewParentPosTop < workshopModalHeight) {
          workshopModal.css({
            'right': previewParentWidth - 25 + 'px',
            'top': 0,
            'display': 'block'
          });
        } else if (previewParentPosLeft < leftLimit && previewParentPosTop > workshopModalHeight) {
          workshopModal.css({
            'left': previewParentWidth - 25 + 'px',
            'bottom': 0,
            'display': 'block'
          });
        } else if (previewParentPosLeft < leftLimit && previewParentPosTop < workshopModalHeight) {
          workshopModal.css({
            'left': previewParentWidth - 25 + 'px',
            'top': 0,
            'display': 'block'
          });
        }

        workshopModal.on('mouseleave', function(e) {
          var previewOffset = preview.offset(),
              previewWitdh = parseInt(preview.css('width')),
              previewHeight = parseInt(preview.css('height'));
              relX = e.pageX,
              relY = e.pageY,
              previewXLimit = previewOffset.left + previewWitdh,
              previewYLimit = previewOffset.top + previewHeight;

          if (!(relX >= previewOffset.left && relX <= previewXLimit && relY >= previewOffset.top && relY <= previewYLimit)) {
            $(this).css('display', 'none');
          }
        });

        preview.on('mouseleave', function(e) {
          var modalOffset = workshopModal.offset(),
              relX = e.pageX,
              relY = e.pageY,
              modalXLimit = modalOffset.left + workshopModalWitdh,
              modalYLimit = modalOffset.top + workshopModalHeight;

          if (!(relX >= modalOffset.left && relX <= modalXLimit && relY >= modalOffset.top && relY <= modalYLimit)) {
            workshopModal.css('display', 'none');
          }

        });
      });
    });
  }
});
