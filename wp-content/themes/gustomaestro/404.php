<?php get_header(); ?>
<main class="main promo--404">
  <!-- promo-->
  <section class="promo">
    <div class="section__container promo__container">
      <div class="promo__offer"><i class="spoon-fork"></i>
        <h1 class="section__header promo__title">Упс, такой страницы нет</h1>
        <a href="<?php echo home_url(); ?>" class="btn promo__btn">Перейти на главную</a>
      </div>
    </div>
  </section>
</main>
<?php get_footer(); ?>
