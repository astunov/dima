<?php get_header(); ?>
<main class="main">
  <!-- workshops-->
  <div class="workshops">
    <div class="section__container workshops__container">
      <nav class="workshops__nav">
        <ul class="workshops__tab-radio-list">
          <li class="section__subtitle workshops__title"><a href="#" class="active">Все мастер-классы</a></li>
          <li class="section__subtitle workshops__title"><a data-tax="adult" href="#">Взрослые</a></li>
          <li class="section__subtitle workshops__title"><a data-tax="children" href="#">Детские</a></li>
        </ul>
        <div class="workshops__date section__text selectbox__wrapper">
          <div id="modalSelectbox" class="selectbox modal__selectbox"><span class="selectbox__text jsSelectboxResult">Предстоящие</span></div>
          <ul class="selectbox__dropdown">
          </ul>
        </div>
      </nav>

      <?php
      $args = array(
        'post_type' => 'cpt_workshops',
        'tax_query' => array(
          'relation' => 'AND',
          array(
            'taxonomy' => 'workshop-type',
            'field'    => 'slug',
            'terms'    => 'private',
            'operator' => 'NOT IN',
          )
        ),
        'posts_per_page' => 16
      );
      $mcPreviewTitle = '';
      $showCurrent = true;
      include(locate_template('template-parts/template-mc-preview-loop.php'));
      ?>
      <!-- pagination-->
      <!-- <nav class="pagination">
        <ul class="pagination__list">
          <li class="pagination__item"><a href="#" class="pagination__arrow"></a></li>
          <li class="pagination__item"><a href="#">1</a></li>
          <li class="pagination__item"><a href="#" class="active">2</a></li>
          <li class="pagination__item"><a href="#">3</a></li>
          <li class="pagination__item">...</li>
          <li class="pagination__item"><a href="#">12</a></li>
          <li class="pagination__item"><a href="#" class="pagination__arrow pagination__arrow--left"></a></li>
        </ul>
      </nav> -->
    </div>
  </div>

  <?php
  $postID = 81; // Превью «На мастер классах»
  $btnTitle = '';
  include(locate_template('template-parts/template-image-text--right.php'));
  ?>

  <?php include(locate_template('template-parts/template-contacts.php')); ?>

  <div id="modalWaitingList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
    <div role="document" class="modal-dialog">
      <div class="modal-content">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body">
          <h2 class="section__subtitle modal__title modal__subtitle--big-padding">Записаться в лист ожидания</h2>
          <form id="formWaitingList" class="modal__form">
            <input type="text" placeholder="Имя и Фамилия" class="modal__input">
            <input type="tel" placeholder="Телефон" class="modal__input">
            <input type="submit" value="Записаться" class="btn modal__submit">
          </form>
        </div>
      </div>
    </div>
  </div>

  <?php include(locate_template('template-parts/template-modal-order.php')); ?>
</main>
<?php get_footer(); ?>
