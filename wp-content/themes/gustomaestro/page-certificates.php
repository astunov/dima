<?php get_header(); ?>
<main class="main">
  <!-- certificates-->
  <div class="certificates">
    <div class="section__container certificates__container">
      <!-- certificates-promo-->
      <h2 class="section__title certificates__header">Подарочные сертификаты</h2>
      <div class="certificates__promo">
        <div class="certificates__promo-image"><img src="<?php bloginfo('template_directory'); ?>/images/certificate.jpg" alt=""></div>
        <div class="certificates__promo-content">
          <p class="section__text certificates__text">Если вы хотите сделать незабываемый подарок и порадовать близкого человека. Предлагаем отличную идею оригинального подарка на любой праздник – сертификат на посещение любого мастер-класса кулинарной школы Gustomaestro!</p>
          <h4 class="section__text certificates__title">Сертификат бывает:</h4>
          <p class="section__text certificates__text">любого номинала (минимальный – 3000 рублей) – в таком случае гость сможет посетить мастер- класс, стоимость которого не больше номинала сертификата. <br> В случае если стоимость мастер-класса больше номинала сертификата, гость может доплатить разницу на месте и посетить интересующий мастер-класс. В случае, если стоимость мастер-класса меньше- остаток может пойти в счет оплаты следующего мастер-класса.</p>
          <p class="section__text certificates__text certificates__text--marked">Стоимость сертификата на любой мастер класс – 5000 рублей.</p>
        </div>
      </div>
      <!-- certificates-list-->
      <ul class="certificates__list">
        <li class="certificates__item">
          <div class="certificates__item-image"><img src="<?php bloginfo('template_directory'); ?>/images/certificate.jpg" alt=""></div>
          <div class="certificates__item-content">
            <h4 class="section__text certificates__item-title">Сертификат любого номинала</h4><a href="#" data-toggle="modal" data-target="#modalCertificateSchool" class="section__text link certificates__item-link">Оформить в GUSTOMAESTRO</a>
            <p class="section__text certificates__item-text">Подарить в электронном виде на почту</p><a href="#" data-toggle="modal" data-target="#modalCurrentCertificate" class="btn certificates__btn">Подарить</a>
          </div>
        </li>
        <li class="certificates__item">
          <div class="certificates__item-image"><img src="<?php bloginfo('template_directory'); ?>/images/certificate.jpg" alt=""></div>
          <div class="certificates__item-content">
            <h4 class="section__text certificates__item-title">Сертификат на любой мастер-класс</h4><a href="#" data-toggle="modal" data-target="#modalCertificateSchool" class="section__text link certificates__item-link">Оформить в GUSTOMAESTRO</a>
            <p class="section__text certificates__item-text">Подарить в электронном виде на почту</p><a href="#" data-toggle="modal" data-target="#modalCustomCertificate" class="btn certificates__btn">Подарить</a>
          </div>
        </li>
        <li class="certificates__item">
          <div class="certificates__item-image"><img src="<?php bloginfo('template_directory'); ?>/images/certificate.jpg" alt=""></div>
          <div class="certificates__item-content">
            <h4 class="section__text certificates__item-title">Сертификат на конкретный мастер-класс</h4><a href="#" data-toggle="modal" data-target="#modalCertificateSchool" class="section__text link certificates__item-link">Оформить в GUSTOMAESTRO</a>
            <p class="section__text certificates__item-text">Подарить в электронном виде на почту</p><a href="#" class="btn certificates__btn">Подарить</a>
          </div>
        </li>
      </ul>
      <!-- certificates-rules-->
      <div class="certificates-rules">
        <h3 class="section__subtitle certificates-rules__title">Правила активации сертификатов:</h3>
        <ul class="section__text certificates-rules__list">
          <li class="certificates-rules__item">Подарочный сертификат действителен в течение указанного на нем срока</li>
          <li class="certificates-rules__item">Подарочный сертификат является подтверждением внесения авансового платежа и предоставляет владельцу право на посещение кулинарного мастер-класса GUSTOMAESTRO на сумму, эквивалентную номиналу подарочного сертификата или на мастер-класс, название и дата которого указана в сертификате</li>
          <li class="certificates-rules__item">При реализации подарочного сертификата общей стоимостью ниже его номинала разница в денежном эквиваленте не компенсируется</li>
          <li class="certificates-rules__item">При реализации подарочного сертификата общей стоимостью выше его номинала разница доплачивается владельцем сертификата</li>
          <li class="certificates-rules__item">Подарочный сертификат не подлежит возврату и обмену на денежные средства и не восстанавливается в результате утраты</li>
          <li class="certificates-rules__item">Воспользоваться сертификатом можно только при его предъявлении</li>
        </ul>
      </div>
    </div>
  </div>
  <div id="modalCertificateSchool" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
    <div role="document" class="modal-dialog">
      <div class="modal-content">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body">
          <h2 class="section__subtitle modal__title modal__subtitle--big-padding">Получить сертификат в школе</h2>
          <form id="formCertificateSchool" class="modal__form">
            <input type="text" placeholder="Имя и Фимилия" class="modal__input">
            <input type="tel" placeholder="Телефон" class="modal__input">
            <input type="submit" value="Заказать и получить в школе" class="btn modal__submit">
          </form>
        </div>
      </div>
    </div>
  </div>
  <div id="modalCurrentCertificate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
    <div role="document" class="modal-dialog">
      <div class="modal-content">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body">
          <h2 class="section__subtitle modal__title modal__subtitle--big-padding">Подарить сертификат на определенный мастер-класс</h2>
          <div class="selectbox__wrapper">
            <div id="modalSelectbox" class="selectbox modal__selectbox"><span class="selectbox__text jsSelectboxResult">Конюшня</span></div>
            <ul class="selectbox__dropdown">
              <li data-date="Сб 10 января 2016" data-time="19:00" data-price="3700 руб." class="selectbox__item">
                <p class="selectbox__text">Пирожок</p>
              </li>
              <li data-date="Пт 18 марта 2015" data-time="12:00" data-price="1700 руб." class="selectbox__item">
                <p class="selectbox__text">С капусткой</p>
              </li>
              <li data-date="Чт 17 января 2015" data-time="19:00" data-price="3700 руб." class="selectbox__item">
                <p class="selectbox__text">Но не красный</p>
              </li>
              <li data-date="Чт 17 января 2015" data-time="19:00" data-price="3700 руб." class="selectbox__item">
                <p class="selectbox__text">С капусткой</p>
              </li>
              <li data-date="Чт 17 января 2015" data-time="19:00" data-price="3700 руб." class="selectbox__item">
                <p class="selectbox__text">Но не красный</p>
              </li>
              <li data-date="Чт 17 января 2015" data-time="19:00" data-price="3700 руб." class="selectbox__item">
                <p class="selectbox__text">С капусткой</p>
              </li>
              <li data-date="Чт 17 января 2015" data-time="19:00" data-price="3700 руб." class="selectbox__item">
                <p class="selectbox__text">Но не красный</p>
              </li>
              <li data-date="Чт 17 января 2015" data-time="19:00" data-price="3700 руб." class="selectbox__item">
                <p class="selectbox__text">С капусткой</p>
              </li>
              <li data-date="Чт 17 января 2015" data-time="19:00" data-price="3700 руб." class="selectbox__item">
                <p class="selectbox__text">Но не красный</p>
              </li>
            </ul>
          </div>
          <div class="modal__info modal__info--no-padding">
            <ul class="section__text modal__info-list">
              <li class="modal__info-item"><span id="selectDate" class="modal__info-item--marked">17 Ноября 2015</span></li>
              <li class="modal__info-item"><span id="selectTime" class="modal__info-item--marked">в 19:00</span></li>
              <li class="modal__info-item"><span id="selectPrice" class="modal__info-item--marked modal__info-item--bold">3700 руб.</span></li>
            </ul>
          </div>
          <form id="formCurrentCertificate" class="modal__form">
            <input type="text" placeholder="Кому" class="modal__input">
            <input type="text" placeholder="От кого" class="modal__input">
            <input type="tel" name="tel" placeholder="Телефон получателя" class="modal__input">
            <input type="email" placeholder="E-mail получателя" class="modal__input">
            <input type="email" placeholder="Ваш E-mail" class="modal__input">
            <input type="submit" value="Оплатить и подарить сертификат" class="btn modal__submit">
            <p class="section__text section__text--small modal__text--explain">После нажатия на кнопку «Записаться и оплатить» вы будете перенаправлены на страницу оплаты ЯНДЕКС.КАССЫ. После оплаты отправим электронные билеты на указанный email.</p>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div id="modalCustomCertificate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
    <div role="document" class="modal-dialog modal-dialog--custom-certificate">
      <div class="modal-content">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body">
          <h2 class="section__subtitle modal__title modal__subtitle--big-padding">Сертификат на
            <div class="modal__custom-rubles"><span id="customRublesResult" class="modal__custom-rubles-result">3000</span>
              <div class="modal__custom-rubles-changes"><span id="customRublesPlus" class="modal__custom-rubles-change modal__custom-rubles-change--plus"></span> <span id="customRublesMinus" class="modal__custom-rubles-change modal__custom-rubles-change--minus"></span></div>
            </div> рублей
          </h2>
          <form id="formCustomCertificate" class="modal__form">
            <input type="text" placeholder="Кому" class="modal__input">
            <input type="text" placeholder="От кого" class="modal__input">
            <input type="tel" name="tel" placeholder="Телефон получателя" class="modal__input">
            <input type="email" placeholder="E-mail получателя" class="modal__input">
            <input type="email" placeholder="Ваш E-mail" class="modal__input">
            <input type="submit" value="Оплатить и подарить сертификат" class="btn modal__submit">
            <p class="section__text section__text--small modal__text--explain">После нажатия на кнопку «Записаться и оплатить» вы будете перенаправлены на страницу оплаты ЯНДЕКС.КАССЫ. После оплаты отправим электронные билеты на указанный email.</p>
          </form>
        </div>
      </div>
    </div>
  </div>

  <?php include(locate_template('template-parts/template-contacts.php')) ?>
</main>
<?php get_footer(); ?>
