<?php
/**
 * gustomaestro functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gustomaestro
 */

if ( ! function_exists( 'gustomaestro_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function gustomaestro_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on gustomaestro, use a find and replace
	 * to change 'gustomaestro' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'gustomaestro', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'gustomaestro' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'gustomaestro_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'gustomaestro_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gustomaestro_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'gustomaestro_content_width', 640 );
}
add_action( 'after_setup_theme', 'gustomaestro_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gustomaestro_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gustomaestro' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gustomaestro' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'gustomaestro_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gustomaestro_scripts() {
	wp_enqueue_style( 'gustomaestro-style', get_stylesheet_uri() );

	wp_enqueue_script( 'gustomaestro-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'gustomaestro-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gustomaestro_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/**
 * Мои функции
 */

// Отправка писем
function ajax_auth_init() {
  wp_register_script('ajax-auth-script', get_template_directory_uri() . '/js/ajax-auth-script.js', array('jquery'));
  wp_enqueue_script('ajax-auth-script');

  wp_localize_script('ajax-auth-script', 'ajax_auth_object', array(
    'ajaxurl' => admin_url('admin-ajax.php')
  ));

  add_action('wp_ajax_nopriv_ajaxorderworkshop', 'ajaxOrderWorkshop');
  add_action('wp_ajax_nopriv_ajaxorderpreviewevent', 'ajaxOrderPreviewEvent');
}

// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
  add_action('init', 'ajax_auth_init');
}

function ajaxOrderWorkshop() {
  $name = $_POST['name'];
  $tel = $_POST['tel'];
  $email = $_POST['email'];

  echo json_encode(array('message' => "$name, регистрация завершена! Мы выслали пароль на почту $email, $tel"));

  die();
}

function ajaxOrderPreviewEvent() {
  $name = $_POST['name'];
  $tel = $_POST['tel'];
  $comment = $_POST['comment'];

  echo json_encode(array('message' => "$name, регистрация завершена! Мы выслали пароль на почту $comment, $tel"));

  die();
}


// Календарь
function ajax_calendar_init() {
  wp_register_script('underscore-min', 'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js', false, false, true);
  wp_enqueue_script('underscore-min');

  wp_register_script('moment-js', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js', false, false, true);
  wp_enqueue_script('moment-js');

  wp_register_script('calendar-js', get_template_directory_uri() . '/js/clndr.js', false, false, true);
  wp_enqueue_script('calendar-js');

  wp_register_script('calendar-custom-field', get_template_directory_uri() . '/js/calendar-custom-field.js', false, false, true);
  wp_enqueue_script('calendar-custom-field');


  wp_localize_script('calendar-custom-field', 'calendar_custom_object', array(
    'ajaxurl' => admin_url('admin-ajax.php')
  ));

  add_action('wp_ajax_ajaxgetfield', 'getCustomField');
  add_action('wp_ajax_nopriv_ajaxgetfield', 'getCustomField');

  add_action('wp_ajax_ajaxgetworkshop', 'getCustomWorkshop');
  add_action('wp_ajax_nopriv_ajaxgetworkshop', 'getCustomWorkshop');

  add_action('wp_ajax_testajaxfield', 'testAjaxField');
  add_action('wp_ajax_nopriv_testajaxfield', 'testAjaxField');
}
add_action('init', 'ajax_calendar_init');

// Подгрузка базы мастер-классов
function getCustomField() {
  $dateArray = array();
  $postIDs = array();
  $mounthInNumbers = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');

  $the_query = new WP_Query(array('post_type' => 'cpt_workshops'));

  while ( $the_query->have_posts() ) : $the_query->the_post();

  $getPost = get_the_ID();
  $getDate = get_field('workshop__date');
  $strArray = explode(' ', $getDate);

  $dayToInt = intval($strArray[0]);
  if ($dayToInt > 0 && $dayToInt < 10) {
    $day = sprintf("%02d", $dayToInt);
  } else {
    $day = $dayToInt;
  }

  $year = intval($strArray[2]);

  $month = $strArray[1];
  foreach($mounthInNumbers as $key => $value) {
    if ($value == $month) {
      $month = $key + 1;

      if ($month > 0 && $month < 10) {
        $month = sprintf("%02d", $month);
      }

      break;
    }
  }

  array_push($dateArray, "$year-$month-$day");
  array_push($postIDs, $getPost);

  endwhile;

  wp_reset_postdata();

  echo json_encode(array('dates' => $dateArray, 'IDs' => $postIDs));

  die();
}

// Выборка мастер-классов по текущему месяцу в календаре
function getCustomWorkshop() {
  $currentPost = $_POST['ID'];
  $currentDate = $_POST['date'];

  $the_query = new WP_Query(array('post_type' => 'cpt_workshops', 'p' => $currentPost));
  $output = '';
  $outputModal = '';

  $the_query->the_post();

  date_default_timezone_set('Europe/Moscow');
  $postDate = strtotime($currentDate);
  $todayDate = strtotime(date("Y-m-d"));

  $terms = get_the_terms( $post->ID, 'workshop-type');
  $checkTerm = false; // Не показываем частные мероприятия

  if ($terms && !is_wp_error($terms)) {
    foreach ($terms as $term) {
      if ($term->slug == 'private') {
        $checkTerm = true;
      }
    }
  }

  if ($todayDate > $postDate || $checkTerm == true) {
    $output .= '<div class="calendar-workshop-preview unactive"><h3 class="calendar-subtitle">' . get_the_title() . '</h3><span class="calendar-time">в ' . get_field('workshop__time') . '</span></div>';

    echo json_encode(array('postTitle' => $output, 'date' => $currentDate));
  } else {
    $output .= '<div class="calendar-workshop-preview"><h3 class="calendar-subtitle">' . get_the_title() . '</h3><span class="calendar-time">в ' . get_field('workshop__time') . '</span></div>';

    $outputModal .= '<div class="calendar-modal"><li class="mc-preview__item"><a href="' . get_permalink() . '" class="target-link"></a><h4 class="section__text section__text--medium mc-preview__subtitle">' . get_the_title() . '</h4><div class="mc-preview__image mc-preview__image--bg">' . get_the_post_thumbnail() . '<a href="' . get_permalink() . '" class="btn mc-preview__btn mc-preview__btn--more">Подробнее</a><a href="#" data-toggle="modal" data-target="#modalWorkshopOrder" class="btn mc-preview__btn mc-preview__btn--enroll" data-title="' . get_the_title() . '" data-date="' . get_field('workshop__date') . '" data-time="' . get_field('workshop__time') . '" data-cost="' . get_field('workshop__cost') . '" data-billet-left="' . get_field('workshop__billet-left') . '">Записаться</a></div><div class="mc-preview__content">';

      if (have_rows('workshop__dishes-list')) {
        $outputModal .= '<div class="mc-preview__recipe"><span class="section__text section__text--small mc-preview__recipe-title">Приготовим:</span><ol class="section__text section__text--small mc-preview__recipe-list">';

          while (have_rows('workshop__dishes-list')) {
            the_row();
            $outputModal .= '<li class="mc-preview__recipe-item">' . get_sub_field('dishes-list__dish') . '</li>';
          }

          $outputModal .= '</ol></div>';
      }

      $outputModal .= '</div>';

      if (get_field('workshop__billet-left') !== '0') {

        $outputModal .= '<span class="section__text section__text--medium mc-preview__billet-left">Осталось <span class="mc-preview__info-item--marked jsCurrentWord">' . get_field('workshop__billet-left') . '</span></span>';
      } else if (get_field('workshop__billet-left') == '0') {
        $outputModal .= '<span class="section__text section__text--medium mc-preview__billet-left mc-preview--noplaces">На мастер-класс запись закрыта</span>';
      }

      $outputModal .= '<div class="mc-preview__info"><ul class="section__text section__text--medium mc-preview__info-list"><li class="mc-preview__info-item"><span class="mc-preview__info-item--marked jsWorkshopDate">' . get_field('workshop__date') . '</span></li><li class="mc-preview__info-item"><span class="mc-preview__info-item--marked">в ' . get_field('workshop__time') . '</span></li><li class="mc-preview__info-item"><span class="mc-preview__info-item--marked mc-preview__info-item--bold">' . get_field('workshop__cost') . ' руб.</span></li></ul></div></li></div>';

    echo json_encode(array('postTitle' => $output, 'date' => $currentDate, 'modalOutput' => $outputModal, 'active' => true));
  }

  die();
}


// Выборка мастер-классов по тегам таксономий
function ajax_workshop_init() {
  wp_register_script('get-current-workshops', get_template_directory_uri() . '/js/get-current-workshops.js', false, false, true);
  wp_enqueue_script('get-current-workshops');

  wp_localize_script('get-current-workshops', 'workshops_object', array(
    'ajaxurl' => admin_url('admin-ajax.php')
  ));

  add_action('wp_ajax_checkboxslist', 'setCheckboxList');
  add_action('wp_ajax_nopriv_checkboxslist', 'setCheckboxList');

  add_action('wp_ajax_setlistbycheckbox', 'setListByCheckbox');
  add_action('wp_ajax_nopriv_setlistbycheckbox', 'setListByCheckbox');
}
add_action('init', 'ajax_workshop_init');

function setCheckboxList() {
  $postDates = array();
  $args = array(
    'post_type' => 'cpt_workshops',
    'tax_query' => array(
      'relation' => 'AND',
      array(
        'taxonomy' => 'workshop-type',
        'field'    => 'slug',
        'terms'    => 'private',
        'operator' => 'NOT IN',
      )
    )
  );

  $the_query = new WP_Query($args);

  if ($the_query->have_posts()) {

    while ($the_query->have_posts()) {
      $the_query->the_post();

      $getDate = get_field('workshop__date');
      $strArray = explode(' ', $getDate);
      $mounthInNumbers = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');

      $dayToInt = intval($strArray[0]);
      if ($dayToInt > 0 && $dayToInt < 10) {
        $day = sprintf("%02d", $dayToInt);
      } else {
        $day = $dayToInt;
      }

      $year = intval($strArray[2]);

      $month = $strArray[1];
      foreach($mounthInNumbers as $key => $value) {
        if ($value == $month) {
          $month = $key + 1;

          if ($month > 0 && $month < 10) {
            $month = sprintf("%02d", $month);
          }

          break;
        }
      }

      array_push($postDates, "$year-$month");
    }
  }

  sort($postDates, SORT_NUMERIC);
  rsort($postDates);
  $postDatesResult = array_unique($postDates);
  $postDates = array_values($postDatesResult);
  $output = '';

  $output .= '<li class="selectbox__item"><p class="selectbox__text">Предстоящие</p></li>';

  for ($i = 0; $i < count($postDates); $i++) {
    $output .= '<li data-workshop-month="'. $postDates[$i] .'" class="selectbox__item"><p class="selectbox__text">'. $postDates[$i] .'</p></li>';
  }

  echo json_encode(array('selectboxItems' => $output, 'dates' => ""));

  die();
}


function setListByCheckbox() {
  $tax = $_POST['tax'];

  if ($tax !== '') {
    $the_query = new WP_Query(array(
      'post_type' => 'cpt_workshops',
      'workshop-type' => $tax,
      'tax_query' => array(
        'relation' => 'AND',
        array(
          'taxonomy' => 'workshop-type',
          'field'    => 'slug',
          'terms'    => 'private',
          'operator' => 'NOT IN',
        )
      ),
      'posts_per_page' => 16
    ));
  } else {
    $the_query = new WP_Query(array(
      'post_type' => 'cpt_workshops',
      'tax_query' => array(
        'relation' => 'AND',
        array(
          'taxonomy' => 'workshop-type',
          'field'    => 'slug',
          'terms'    => 'private',
          'operator' => 'NOT IN',
        )
      ),
      'posts_per_page' => 16
    ));
  }

  $output = '';

  if ($the_query->have_posts()) {

    $output .= '<ul class="mc-preview__list">';

    while ($the_query->have_posts()) {
      $the_query->the_post();

      $getDate = get_field('workshop__date');
      $strArray = explode(' ', $getDate);
      $mounthInNumbers = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');

      $dayToInt = intval($strArray[0]);
      if ($dayToInt > 0 && $dayToInt < 10) {
        $day = sprintf("%02d", $dayToInt);
      } else {
        $day = $dayToInt;
      }

      $year = intval($strArray[2]);

      $month = $strArray[1];
      foreach($mounthInNumbers as $key => $value) {
        if ($value == $month) {
          $month = $key + 1;

          if ($month > 0 && $month < 10) {
            $month = sprintf("%02d", $month);
          }

          break;
        }
      }

      $currentDate = $_POST['currentDate'];
      $postDate = "$year-$month";

      if ($postDate == $currentDate) {
        $output .= '<li class="mc-preview__item"><a href="' . get_permalink() . '" class="target-link"></a><h4 class="section__text section__text--medium mc-preview__subtitle">' . get_the_title() . '</h4><div class="mc-preview__image mc-preview__image--bg">' . get_the_post_thumbnail() . '<a href="' . get_permalink() . '" class="btn mc-preview__btn mc-preview__btn--more">Подробнее</a><a href="#" data-toggle="modal" data-target="#modalWorkshopOrder" class="btn mc-preview__btn mc-preview__btn--enroll" data-title="' . get_the_title() . '" data-date="' . get_field('workshop__date') . '" data-time="' . get_field('workshop__time') . '" data-cost="' . get_field('workshop__cost') . '" data-billet-left="' . get_field('workshop__billet-left') . '">Записаться</a></div><div class="mc-preview__content">';

        if (have_rows('workshop__dishes-list')) {

          $output .= '<div class="mc-preview__recipe"><span class="section__text section__text--small mc-preview__recipe-title">Приготовим:</span><ol class="section__text section__text--small mc-preview__recipe-list">';

          while (have_rows('workshop__dishes-list')) {
            the_row();

            $output .= '<li class="mc-preview__recipe-item">' . get_sub_field('dishes-list__dish') . '</li>';
          }

          $output .= '</ol></div>';
        }

        $output .= '</div>';

        date_default_timezone_set('Europe/Moscow');
        $currentDate1 = strtotime(date("Y-m-d"));
        $postDate1 = strtotime("$year-$month-$day");

        if ($postDate1 < $currentDate1) {
          $output .= '<span class="section__text section__text--medium mc-preview__billet-left mc-preview--closed" data-recipe="' . get_field('mc__attachment--recipe') . '" data-album="' . get_field('mc__attachment--album') . '">Мастер-класс уже прошёл</span>';
        } else if (get_field('workshop__billet-left') == '0') {
          $output .= '<span class="section__text section__text--medium mc-preview__billet-left mc-preview--noplaces">На мастер-класс запись закрыта</span>';
        } else if (get_field('workshop__billet-left') !== '0') {
          $output .= '<span class="section__text section__text--medium mc-preview__billet-left">Осталось <span class="mc-preview__info-item--marked jsCurrentWord">' . get_field('workshop__billet-left') . '</span></span>';
        }

        $output .= '<div class="mc-preview__info"><ul class="section__text section__text--medium mc-preview__info-list"><li class="mc-preview__info-item"><span class="mc-preview__info-item--marked jsWorkshopDate">' . get_field('workshop__date') . '</span></li><li class="mc-preview__info-item"><span class="mc-preview__info-item--marked">в ' . get_field('workshop__time') . '</span></li><li class="mc-preview__info-item"><span class="mc-preview__info-item--marked mc-preview__info-item--bold">' . get_field('workshop__cost') . ' руб.</span></li></ul></div></li>';
      }

      if ($currentDate == '') {
        date_default_timezone_set('Europe/Moscow');
        $currentDate = strtotime(date("Y-m-d"));
        $postDate = strtotime("$year-$month-$day");

        if ($postDate > $currentDate) {
          $output .= '<li class="mc-preview__item"><a href="' . get_permalink() . '" class="target-link"></a><h4 class="section__text section__text--medium mc-preview__subtitle">' . get_the_title() . '</h4><div class="mc-preview__image mc-preview__image--bg">' . get_the_post_thumbnail() . '<a href="' . get_permalink() . '" class="btn mc-preview__btn mc-preview__btn--more">Подробнее</a><a href="#" data-toggle="modal" data-target="#modalWorkshopOrder" class="btn mc-preview__btn mc-preview__btn--enroll" data-title="' . get_the_title() . '" data-date="' . get_field('workshop__date') . '" data-time="' . get_field('workshop__time') . '" data-cost="' . get_field('workshop__cost') . '" data-billet-left="' . get_field('workshop__billet-left') . '">Записаться</a></div><div class="mc-preview__content">';

          if (have_rows('workshop__dishes-list')) {

            $output .= '<div class="mc-preview__recipe"><span class="section__text section__text--small mc-preview__recipe-title">Приготовим:</span><ol class="section__text section__text--small mc-preview__recipe-list">';

            while (have_rows('workshop__dishes-list')) {
              the_row();

              $output .= '<li class="mc-preview__recipe-item">' . get_sub_field('dishes-list__dish') . '</li>';
            }

            $output .= '</ol></div>';
          }

          $output .= '</div>';

          if (get_field('workshop__billet-left') !== '0') {

            $output .= '<span class="section__text section__text--medium mc-preview__billet-left">Осталось <span class="mc-preview__info-item--marked jsCurrentWord">' . get_field('workshop__billet-left') . '</span></span>';
          } else if (get_field('workshop__billet-left') == '0') {
            $output .= '<span class="section__text section__text--medium mc-preview__billet-left mc-preview--noplaces">На мастер-класс запись закрыта</span>';
          }

          $output .= '<div class="mc-preview__info"><ul class="section__text section__text--medium mc-preview__info-list"><li class="mc-preview__info-item"><span class="mc-preview__info-item--marked jsWorkshopDate">' . get_field('workshop__date') . '</span></li><li class="mc-preview__info-item"><span class="mc-preview__info-item--marked">в ' . get_field('workshop__time') . '</span></li><li class="mc-preview__info-item"><span class="mc-preview__info-item--marked mc-preview__info-item--bold">' . get_field('workshop__cost') . ' руб.</span></li></ul></div></li>';
        }
      }
    }

    wp_reset_postdata();

    if ($output == '<ul class="mc-preview__list">') {
      $output .= '<li><h3 class="section__subtitle">Мастер-классов нет</h3></li>';
    }

    $output .= '</ul>';
  }

  echo json_encode(array('worksoplist' => $output));

  die();
}

