<?php get_header(); ?>
<main class="main">
  <?php include(locate_template('template-parts/template-breadcrumbs.php')); ?>
  <!-- about-chief-->
  <div class="about-chief">
    <div class="section__container about-chief__container">
      <div class="about-chief__image"><img src="<?php the_field('chief__image'); ?>"></div>
      <div class="about-chief__content">
        <h3 class="section__subtitle about-chief__title"><?php the_title(); ?></h3>
        <span class="section__text section__text--small about-chief__subtitle"><?php the_field('chief__kitchen'); ?></span>
        <p class="section__text about-chief__text"><?php the_field('chief__text'); ?></p>
      </div>
    </div>
  </div>
  <!-- main-dishes-->
  <div class="main-dishes">
    <div class="section__container main-dishes__container">
      <h3 class="section__subtitle main-dishes__title">Фирменные блюда шефа</h3>
      <?php
      if (have_rows('chief__main-dishes')) {
      ?>
      <ul class="main-dishes__list">
        <?php
        while (have_rows('chief__main-dishes')) {
          the_row();
          ?>
          <li class="main-dishes__item">
            <figure>
              <div class="main-dishes__image"><img src="<?php the_sub_field('main-dishes__image'); ?>"></div>
              <figcaption class="section__text main-dishes__text"><?php the_sub_field('main-dishes__text'); ?></figcaption>
            </figure>
          </li>
        <?php
        }
        ?>
      </ul>
      <?php
      }
      ?>
    </div>
  </div>

  <?php
  $args = array(
    'post_type' => 'cpt_workshops',
    'liked_workshops' => 'chief',
    'posts_per_page' => 3
  );
  $makeSubtitle = true;
  $mcPreviewTitle = 'Проводит мастер-классы';
  include(locate_template('template-parts/template-mc-preview-loop.php'));
  ?>
</main>
<?php include(locate_template('template-parts/template-modal-order.php')); ?>
<?php get_footer(); ?>
