<?php
$args = array(
  'post_type' => 'page-preview',
  'p' => $postID // Пост «На праздниках и корпоративах»
);
$query = new WP_Query($args);

if ($query->have_posts()) {
  $query->the_post();
?>
  <!-- col-text-col-image-->
  <section class="col-text-col-image col-text-col-image--image-left" style="background: url(<?php the_field('page-preview__image'); ?>) no-repeat;">
    <div class="section__container col-text-col-image__container">
      <div class="col-text-col-image__image">
        <img src="<?php the_field('page-preview__image'); ?>">
      </div>
      <div class="col-text-col-image__content">
        <h3 class="section__subtitle col-text-col-image__title"><?php the_title(); ?></h3>
        <p class="section__text section__text--medium col-text-col-image__text"><?php the_field('page-preview__text'); ?></p>
        <?php
        if ($btnTitle !== '') {
        ?>
          <a href="<?php echo get_permalink(get_page_by_path($permalinkPageName)); ?>" class="btn col-text-col-image__btn"><?php echo $btnTitle; ?></a>
        <?php
        }
        ?>
      </div>
    </div>
  </section>
<?php
}
?>
