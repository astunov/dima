<div id="modalWaitingList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body">
        <h2 class="section__subtitle modal__title modal__subtitle--big-padding">Записаться в лист ожидания</h2>
         <?php echo do_shortcode('[contact-form-7 id="267" title="Записаться в лист ожидания" html_id="formWaitingList" html_class="modal__form"]') ?>
      </div>
    </div>
  </div>
</div>
