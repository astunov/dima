<!-- breadcrumbs-->
<div class="breadcrumbs breadcrumbs--mc-promo">
  <div class="section__container breadcrumbs__container">
    <ul class="section__text section__text--small breadcrumbs__list">
      <?php if(function_exists('bcn_display')) {
        bcn_display();
      } ?>
    </ul>
  </div>
</div>
