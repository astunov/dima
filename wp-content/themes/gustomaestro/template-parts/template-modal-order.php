<div id="modalWorkshopOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body">
        <h2 class="section__subtitle modal__title">Записаться на мастер класс</h2>
        <h3 class="section__text modal__subtitle" id="modalTitle"></h3>
        <div class="modal__info">
          <ul class="section__text modal__info-list">
            <li class="modal__info-item"><span class="modal__info-item--marked" id="modalDate"></span></li>
            <li class="modal__info-item"><span class="modal__info-item--marked" id="modalTime"></span></li>
            <li class="modal__info-item"><span id="currentPrice" class="modal__info-item--marked modal__info-item--bold"></span></li>
          </ul>
          <div class="modal__info-check">
            <div class="modal__left-side">
              <p class="section__text section__text--small modal__check-title">Количество участников</p>
            </div>
            <div class="modal__right-side">
              <div class="modal__choise-list"><span id="minusPerson" class="modal__choise modal__choise--minus"></span>
                <input id="personResult" type="text" value="2" class="modal__choise modal__choise--number"/><span id="plusPerson" class="modal__choise modal__choise--plus"></span>
              </div>
              <p class="section__text section__text--small modal__billet-left">Осталось <span id="billetLeft">3 </span></p>
            </div>
          </div>
          <div class="modal__info-check modal__info-check--big-padding-bottom">
            <div class="modal__left-side">
              <p class="section__text section__text--small modal__check-title">Итого оплатить</p>
            </div>
            <div class="modal__right-side"><span id="resultPrice" class="section__text section__text--bold modal__result"></span></div>
          </div>
          <form id="formOrder" class="modal__form">
            <input type="text" name="name" placeholder="Имя и Фамилия" class="modal__input"/>
            <input type="tel" name="tel" placeholder="Телефон" class="modal__input"/>
            <input type="text" name="email" placeholder="E-mail" class="modal__input"/>
            <input type="submit" value="Записаться и оплатить" class="btn modal__submit"/>
            <p class="section__text section__text--small modal__text--explain">После нажатия на кнопку «Записаться и оплатить» вы будете перенаправлены на страницу оплаты ЯНДЕКС.КАССЫ. После оплаты отправим электронные билеты на указанный email.</p>
          </form>
          <h3 class="section__text section__text--bold section__text--serif modal__subtitle--big-padding">У меня есть сертификат</h3>
          <form id="formCertificate" class="modal__form">
            <input type="text" placeholder="Номер сертификата" class="modal__input"/>
            <input type="submit" value="Записаться" class="btn modal__submit"/>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
