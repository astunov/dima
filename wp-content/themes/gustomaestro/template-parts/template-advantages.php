<!-- advantages-->
<section class="advantages">
  <div class="section__container advantages__container">
    <?php
    if ($advantagesTitle == '') {
      if ($advantagesTitleBig == true) {
        ?>
        <h3 class="section__title advantages__title">У нас на мероприятиях</h3>
        <?php
      } else {
        ?>
        <h3 class="section__subtitle advantages__title">У нас на мероприятиях</h3>
        <?php
      }
      ?>
      <?php
    } else {
      ?>
      <h3 class="section__subtitle advantages__title"><?php echo $advantagesTitle; ?></h3>
      <?php
    }
    ?>
    <ul class="advantages__list">
      <li class="advantages__item">
        <div class="advantages__image advantages__image--chief"></div>
        <p class="section__text advantages__text">Каждый гость полностью участвует в приготовлении своего блюда, минуя при этом процессы заготовки продуктов</p>
      </li>
      <li class="advantages__item">
        <div class="advantages__image advantages__image--dish"></div>
        <p class="section__text advantages__text">Можно заказывать еду и напитки в ресторане GUSTO. На мастер-классе работают официанты</p>
      </li>
      <li class="advantages__item">
        <div class="advantages__image advantages__image--plate"></div>
        <p class="section__text advantages__text">Высокое техническое оснащение: итальянская техника SMEG, фарфор ЛФЗ, посуда Villeroy&Boch</p>
      </li>
    </ul>
  </div>
</section>
