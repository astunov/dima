<?php get_header(); ?>
<main class="main">
  <?php include(locate_template('template-parts/template-breadcrumbs.php')); ?>
  <!-- mc-promo-->
  <div class="mc-promo mc-promo--event">
    <div class="section__container mc-promo__container">
      <div class="mc-promo__wrapper mc-promo__wrapper--left">
        <h2 class="section__subtitle mc-promo__title">Праздники и корпоративы для взрослых</h2>
        <div class="mc-promo__info mc-promo__info--event">
          <ul class="section__text mc-promo__info-list">
            <li class="mc-promo__info-item">В любой свободный день</li>
            <li class="mc-promo__info-item"><a href="#" class="link">посмотреть в календаре</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="section__container mc-promo__container">
      <div class="mc-promo__wrapper mc-promo__wrapper--left">
        <div class="mc-promo__image"><img src="<?php the_field('cpt-event__image'); ?>" alt=""></div>
      </div>
      <div class="mc-promo__wrapper mc-promo__wrapper--right">
        <p class="section__text section__text--medium mc-promo__text"><?php the_field('cpt-event__text'); ?></p>
        <a href="#" data-toggle="modal" data-target="#modalOrderEvent" class="btn">Заказать мероприятие</a>
      </div>
    </div>
  </div>

  <!-- gallery-->
  <div class="gallery">
    <div class="section__container gallery__container">
      <h2 class="section__subtitle gallery__title">Как проходят корпоративы и личные праздники</h2>
      <div class="gallery__wrapper">
        <?php
        $images = get_field('gallery');
        if( $images ): ?>
          <ul class="gallery__list">
            <?php foreach( $images as $image ): ?>
              <li class="gallery__item">
                <a class="swipebox" href="<?php echo $image['url']; ?>">
                  <div class="gallery__image"><img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>

  <div class="calendar" id="calendar">
    <div class="section__container calendar__container">
      <h3 class="section__subtitle calendar__title">Организуем ваше мероприятие в любую свободную дату</h3>
      <div class="cal1"></div>
      <a href="#" class="btn calendar__btn" data-toggle="modal" data-target="#modalOrderPreviewEvent">Заказать организацию мероприятия</a>
    </div>
  </div>

  <?php include(locate_template('template-parts/template-contacts.php')); ?>
</main>
<?php include(locate_template('template-parts/template.modal-order-preview-event.php')); ?>
<?php get_footer(); ?>
