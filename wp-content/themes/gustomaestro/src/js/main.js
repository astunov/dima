'use strict';

$(document).ready(function() {

  // Открывает мобильное меню
  var openMobileMenu = function(btn, menu) {
    $(btn).on('click', function(e) {
      e.preventDefault();

      if ($(this).attr('data-open') == 'false') {
        $(this).attr('data-open', 'true').addClass('open');

        $(menu).slideDown(300);
      } else {
        $(this).attr('data-open', 'false').removeClass('open');

        $(menu).slideUp(300);
      }
    });
  }

  // Выравнивает изображения по центру контейнера
  var imageToCenter = function(imageContainer) {
    $(imageContainer).each(function() {
      var boxHeight = $(this).height(),
          boxWidth = $(this).width(),
          boxImage = $(this).find('img'),
          boxImageHeight = boxImage.height(),
          boxImageWidth = boxImage.width();

      if (boxImageHeight > boxImageWidth) {
        boxImage.addClass('full-width').removeClass('full-height');
      } else {
        // boxImage.addClass('full-height').removeClass('full-width');
        boxImage.addClass('full-width').removeClass('full-height');
      }

      var newBoxImageHeight = boxImage.height(),
          newBoxImageWidth = boxImage.width(),
          halfMarginHeight = (newBoxImageHeight - boxHeight) / 2,
          halfMarginWidth = (newBoxImageWidth - boxWidth) / 2;

      if (newBoxImageHeight < newBoxImageWidth) {
        boxImage.css({marginLeft: '-' + halfMarginWidth + 'px', marginTop: 0});//offset left
      } else {
        boxImage.css({marginTop: '-' + halfMarginHeight + 'px', marginLeft: 0});//offset top
      }
    });
  }

  // Открывает селектбокс в модальном окне, подставляет значения
  var openSelectbox = function(selectboxInModal) {
    $(selectboxInModal).on('click', function(e) {
      e.preventDefault();

      var selectbox = $(this),
          selectboxDropdown = $(this).closest('.selectbox__wrapper').find('.selectbox__dropdown'),
          selectboxItems = selectboxDropdown.find('.selectbox__item'),
          selectboxOpen = selectbox.attr('data-open'),
          selectboxResult = selectbox.find('.jsSelectboxResult');

      selectbox.addClass('disabled').attr('data-open', 'true');
      selectboxDropdown.addClass('open');

      selectboxItems.on('click', function() {
        var itemTitle = $(this).text(),
            itemDate = $(this).attr('data-date'),
            itemTime = $(this).attr('data-time'),
            itemPrice = $(this).attr('data-price');

        selectboxResult.text(itemTitle);
        $('#selectDate').text(itemDate);
        $('#selectTime').text('в ' + itemTime);
        $('#selectPrice').text(itemPrice);
      });

      e.stopPropagation();
      $(document).on('click', function() {
        selectbox.removeAttr('data-open').removeClass('disabled');
        selectboxDropdown.removeClass('open');
      });
    });
  }

  // Выствляет первое значение в селектбоксе при открытии модального окна
  var setFirstItemToSelectbox = function(modalId) {
    $('a[data-target="' + modalId + '"]').on('click', function() {
      var selectbox = $('.selectbox'),
          selectboxDropdown = selectbox.closest('.selectbox__wrapper').find('.selectbox__dropdown'),
          selectboxItem = selectboxDropdown.find('.selectbox__item:first-child'),
          selectboxResult = selectbox.find('.jsSelectboxResult'),
          selectDate =  $('#selectDate'),
          selectTime = $('#selectTime'),
          selectPrice = $('#selectPrice');

      selectboxResult.text(selectboxItem.text());
      selectDate.text(selectboxItem.attr('data-date'));
      selectTime.text(selectboxItem.attr('data-time'));
      selectPrice.text(selectboxItem.attr('data-price'));
    });
  }

  // Увеличивает число при кликах на кнопки
  var changeRubles = function(changeElement, plusBtn, minusBtn, step, minValue, resultPriceElement, currentPriceElement) {
    var customRublesResult = $(changeElement),
        customRublesPlus = $(plusBtn),
        customRublesMinus = $(minusBtn),
        resultPrice = $(resultPriceElement),
        currentPrice = $(currentPriceElement).text();

    // Считает сумму денег заказа
    var countSubRubles = function(currentSum) {
      currentPrice = parseInt(currentPrice);

      resultPrice.text(currentSum * currentPrice + ' руб.');
    }

    customRublesResult.on('change', function() {
      var billetLeft = $('#billetLeft'),
          ctrl = $(this).val();

      if (billetLeft.length !== 0) {
        billetLeft = parseInt(billetLeft.text());

        if (ctrl > billetLeft) {
          ctrl = billetLeft;
          $(this).val(billetLeft);
        }
      }

      if (resultPrice && currentPrice) {countSubRubles(ctrl);}
    });

    customRublesPlus.on('click', function() {
      var customRublesCurrent = parseInt(customRublesResult.text()),
          billetLeft = $('#billetLeft'),
          checkVal = false;

      if (isNaN(customRublesCurrent)) {
        customRublesCurrent = parseInt(customRublesResult.val());
        checkVal = true;
      }

      customRublesCurrent += step;

      if (billetLeft.length !== 0) {
        billetLeft = parseInt(billetLeft.text());

        if (customRublesCurrent <= billetLeft) {
          if (checkVal == false) {
            customRublesResult.text(customRublesCurrent);
          } else {
            customRublesResult.val(customRublesCurrent);
          }

          if (resultPrice && currentPrice) {countSubRubles(customRublesCurrent);}
        }
      } else {
        if (checkVal == false) {
          customRublesResult.text(customRublesCurrent);
        } else {
          customRublesResult.val(customRublesCurrent);
        }
      }
    });

    customRublesMinus.on('click', function() {
      var customRublesCurrent = parseInt(customRublesResult.text()),
          checkVal = false;

      if (isNaN(customRublesCurrent)) {
        customRublesCurrent = parseInt(customRublesResult.val());
        checkVal = true;
      }

      customRublesCurrent -= step;

      if (customRublesCurrent >= minValue) {
        if (checkVal == false) {
          customRublesResult.text(customRublesCurrent);
        } else {
          customRublesResult.val(customRublesCurrent);
        }

        if (resultPrice && currentPrice) {countSubRubles(customRublesCurrent);}
      }
    });
  }

  // Емейл-валидация
  function validateEmail(email) {
    var re = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
    return re.test(email);
  }

  // Проверка инпутов на пустоту и правильность емейла
  function validateInputs(formId, ignoreInputs) {
    var checkValid = true,
        formInputs = formId.find('input').not(':input[type=button], :input[type=submit], :input[type=reset]' + ignoreInputs);

    formInputs.each(function() {
      if ($(this).hasClass('error') || $(this).val() == '') {
        if (!$(this).hasClass('error')) {
          $(this).addClass('error');
        }

        checkValid = false;
      } else if ($(this).attr('name') == 'email') {
        if (!validateEmail($(this).val())) {
          $(this).addClass('error');

          checkValid = false;
        }
      }
    });

    return checkValid;
  }

  // Отправка письма
  var sendMail = function(formId) {
    $(formId).on('submit', function(e) {
      e.preventDefault();

      var currentForm = $(this),
          validator = validateInputs(currentForm);

      if (validator == false) {
        return false;
      }

      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'send.php',
        data: currentForm.serialize(),
        beforeSend: function() {
          currentForm.find('input[type="submit"]').attr('disabled', 'disabled');
        },
        success: function(data) {
          currentForm.find('input[type="submit"]').removeAttr('disabled');
        }
      });
    });
  }

  // Задаем шаблон для полей с вводом телефона
  $('input[name="tel"]').mask('+7 (999) 999-99-99',{placeholder:'+7 (___) ___-__-__'});

  // Убираем ошибку инпута при фокусе
  $('input').not(':input[type=button], :input[type=submit], :input[type=reset]').on('focus', function() {
    var currentInput = $(this);

    if (currentInput.hasClass('error')) {
      $(this).removeClass('error');
    }
  });

  // Увеличиваем количество бабла при кликах на кнопки
  changeRubles('#customRublesResult', '#customRublesPlus', '#customRublesMinus', 500, 3000);
  changeRubles('#personResult', '#plusPerson', '#minusPerson', 1, 1, '#resultPrice', '#currentPrice');

  // Селектбоксы в модальных окнах
  setFirstItemToSelectbox('#modalCurrentCertificate');
  openSelectbox('#modalSelectbox');

  // Следит за изменением размеров
  $(window).resize(function() {
    imageToCenter('.mc-preview__image');
    imageToCenter('.gallery__item');
    imageToCenter('.col-text-col-image__image');
    imageToCenter('.mc-promo__image');
    imageToCenter('.main-dishes__image');
    imageToCenter('.comments__image');
    imageToCenter('.certificates__promo-image');
    imageToCenter('.celebration-events__item');
  });

  // Список центрируемых изображений
  imageToCenter('.mc-preview__image');
  imageToCenter('.gallery__item');
  imageToCenter('.col-text-col-image__image');
  imageToCenter('.mc-promo__image');
  imageToCenter('.main-dishes__image');
  imageToCenter('.comments__image');
  imageToCenter('.certificates__promo-image');
  imageToCenter('.celebration-events__item');

  // Мобильное меню
  openMobileMenu('#openMenu', '#mobileMenu');

  // Подставляет нужное склонение «Осталось 3 места»
  var billetLeftItems = $('.jsCurrentWord');

  billetLeftItems.each(function() {
    var billetLeftItem = $(this),
        billetLeftNumber = parseInt(billetLeftItem.text());

    if (billetLeftNumber == 1) {
      billetLeftItem.append(' место');
    } else if (billetLeftNumber > 1 && billetLeftNumber < 5) {
      billetLeftItem.append(' места');
    } else if (billetLeftNumber > 4 && billetLeftNumber < 21) {
      billetLeftItem.append(' мест');
    }
  });

});
