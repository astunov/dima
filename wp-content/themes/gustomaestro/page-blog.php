<?php get_header(); ?>
<!-- promo-->
<section class="promo promo--blog" style="background: url(<?php the_field('promo__background'); ?>) no-repeat center/cover;">
  <div class="section__container promo__container">
    <h1 class="section__header promo__title"><?php the_field('blog__title'); ?></h1>
  </div>
</section>
<main class="main main--articles">
  <div class="section__container main__container">
    <!-- articles-->
    <section class="articles">
      <?php
      $args = array(
        'post_type' => 'cpt_articles',
        'posts_per_page' => 10
      );
      $query = new WP_Query($args);
      if ($query->have_posts()) {
        ?>
        <ul class="articles__list">
        <?php
        while ($query->have_posts()) {
          $query->the_post();
          ?>
          <li class="articles__item">
            <h3 class="section__subtitle articles__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <div class="articles__info">
              <span class="section__text articles__text--serif"><?php the_time('d F Y'); ?></span>
              <span class="section__text articles__text--serif commentsNumber"><?php if(get_comments_number() == 0): echo 'Нет комментариев'; else: echo get_comments_number(); endif; ?></span>
            </div>
            <div class="articles__image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('normal'); ?></a></div>
            <p class="section__text articles__text"><?php echo get_the_excerpt(); ?></p>
            <a href="<?php the_permalink(); ?>" class="section__text link">Читать далее</a>
          </li>
          <?php
        }
        the_posts_navigation();
        ?>
        </ul>
        <?php
      } else {
        get_template_part( 'template-parts/content', 'none' );
      }
      ?>
      <!-- pagination-->
      <!-- <nav class="pagination">
        <ul class="pagination__list">
          <li class="pagination__item"><a href="#" class="pagination__arrow"></a></li>
          <li class="pagination__item"><a href="#">1</a></li>
          <li class="pagination__item"><a href="#" class="active">2</a></li>
          <li class="pagination__item"><a href="#">3</a></li>
          <li class="pagination__item">...</li>
          <li class="pagination__item"><a href="#">12</a></li>
          <li class="pagination__item"><a href="#" class="pagination__arrow pagination__arrow--left"></a></li>
        </ul>
      </nav> -->
    </section>
    <!-- aside-->
    <aside class="aside">
      <?php // get_sidebar(); ?>
      <div class="aside__about-chief">
        <h3 class="section__subtitle aside__title">Фабрицио Фаттучи</h3>
        <p class="section__text aside__text">Приходя в «Gusto», Вы попадаете в гости к знаменитому шеф-повару из Италии Фабрицио Фатуччи. Искусство готовить, по версии мэтра, – это, прежде всего, чувства, эмоции и настроение, которые можно подарить другим. В поисках этих главных ингредиентов, уроженец Рима, он объездил всю Италию. Впитал ароматы Тосканы, Сицилии, Неаполя, и наполнил ими свои блюда. Его кредо - импровизация с душой. Отточив мастерство в десятках лучших заведений, от лондонского гранд-отеля «Savoy» до московского «Forbes», в Петербурге Фабрицио осуществил свою мечту – о «живом» и «тёплом» ресторане, где гости чувствуют себя как дома [...]</p><a href="chief.html" class="section__text link aside__link">Подробнее о Фабрицио</a>
      </div>
      <div class="aside__popular">
        <h3 class="section__subtitle aside__title">Чаще всего читают</h3>
        <ul class="aside__list section__text">
          <li class="aside__item"><a href="article.html" class="link">Россия не для всех</a></li>
          <li class="aside__item"><a href="article.html" class="link">Кажется, будто это было вчера, а прошёл уже целый год!</a></li>
          <li class="aside__item"><a href="article.html" class="link">Фабрицио Фатуччи об эстетике формы и цвета</a></li>
          <li class="aside__item"><a href="article.html" class="link">«Яичница-болтунья»</a></li>
          <li class="aside__item"><a href="article.html" class="link">В гостях у Пятого канала</a></li>
        </ul>
      </div>
      <div class="aside__social">
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?125"></script>
        <!-- VK Widget -->
        <div id="vk_groups"></div>
        <script type="text/javascript">
        VK.Widgets.Group("vk_groups", {redesign: 1, mode: 3, width: "320", height: "400", color1: 'FFFFFF', color2: '5c4739', color3: 'a33208'}, -190545039);
        </script>
      </div>
      <div class="aside__social">
        <div class="fb-page" data-href="https://www.facebook.com/gustospb?ref=hl" data-tabs="timeline" data-width="320" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/gustospb?ref=hl" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/gustospb?ref=hl">Gusto restaurant and Gustomaestro culinary school</a></blockquote></div>
      </div>
    </aside>
  </div>
</main>
<?php get_footer(); ?>
