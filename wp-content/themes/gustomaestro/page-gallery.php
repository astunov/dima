<?php get_header(); ?>
<main class="main main--gallery">
  <!-- gallery-->
  <div class="gallery">
    <div class="section__container gallery__container">
      <h2 class="section__subtitle gallery__title">Интерьер школы</h2>
      <div class="gallery__wrapper">
      <?php
      $images = get_field('gallery__interior');
      if( $images ): ?>
        <ul class="gallery__list">
          <?php foreach( $images as $image ): ?>
            <li class="gallery__item">
              <a class="swipebox" href="<?php echo $image['url']; ?>">
                <div class="gallery__image">
                  <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
      <?php endif; ?>
      </div>
    </div>
  </div>
  <!-- gallery-->
  <div class="gallery">
    <div class="section__container gallery__container">
      <h2 class="section__subtitle gallery__title">Как проходят мастер-классы</h2>
      <div class="gallery__wrapper">
      <?php
      $images = get_field('gallery__workshops');
      if( $images ): ?>
        <ul class="gallery__list">
          <?php foreach( $images as $image ): ?>
            <li class="gallery__item">
              <a class="swipebox" href="<?php echo $image['url']; ?>">
                <div class="gallery__image">
                  <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
      <?php endif; ?>
      </div>
    </div>
  </div>
  <!-- gallery-->
  <div class="gallery">
    <div class="section__container gallery__container">
      <h2 class="section__subtitle gallery__title">Как проходят корпоративы и личные праздники</h2>
      <div class="gallery__wrapper">
      <?php
      $images = get_field('gallery__celebration');
      if( $images ): ?>
        <ul class="gallery__list">
          <?php foreach( $images as $image ): ?>
            <li class="gallery__item">
              <a class="swipebox" href="<?php echo $image['url']; ?>">
                <div class="gallery__image">
                  <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
      <?php endif; ?>
      </div>
    </div>
  </div>
  <?php include(locate_template('template-parts/template-contacts.php')); ?>
</main>
<?php get_footer(); ?>
