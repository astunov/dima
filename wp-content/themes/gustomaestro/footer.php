<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gustomaestro
 */

?>

<!-- footer-->
<div class="footer">
  <div class="section__container footer__container">
    <a href="<?php echo home_url(); ?>">
      <div class="footer__logo"></div>
    </a>
    <nav class="footer__menu">
      <?php wp_nav_menu( array(
        'menu'            => 'primary',
        'container'       => 'false',
        'menu_class'      => 'footer__list',
      )); ?>
      <ul class="footer__labels">
        <li class="footer__labels-item">Copyright © 2015 GUSTOMAESTRO</li>
        <li class="footer__labels-item">Сделано в Astralite</li>
      </ul>
    </nav>
  </div>
</div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/util.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/modal.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.maskedinput.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.swipebox.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>
