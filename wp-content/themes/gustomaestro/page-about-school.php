<?php get_header(); ?>
<main class="main">
  <!-- promo-->
  <section class="promo promo--about" style="background: url(<?php the_field('promo__background'); ?>) no-repeat center/cover;">
    <div class="section__container promo__container">
      <h1 class="section__header promo__title"><?php the_title(); ?></h1>
    </div>
  </section>
  <!-- two-col-text-->
  <div class="two-col-text">
    <div class="section__container two-col-text__container">
      <p class="section__text two-col-text__text"><?php the_field('two-column__first'); ?></p>
      <p class="section__text two-col-text__text"><?php the_field('two-column__second'); ?></p>
    </div>
  </div>

  <?php include(locate_template('template-parts/template-advantages.php')); ?>

  <?php
  $postID = 81; // Превью «На мастер классах»
  $permalinkPageName = 'workshops';
  $btnTitle = 'Посмотреть расписание';
  include(locate_template('template-parts/template-image-text--right.php'));
  ?>

  <?php
  $postID = 82; // Превью «На праздниках и корпоративах»
  $permalinkPageName = 'celebration';
  $btnTitle = 'Узнать больше и заказать';
  include(locate_template('template-parts/template-image-text--left.php'));
  ?>

  <!-- about-chiefs-->
  <section class="about-chiefs">
    <div class="section__container about-chiefs__container">
      <h2 class="section__title about-chiefs__title">О шеф-поварах</h2>
      <?php
      $args = array(
        'post_type' => 'cpt_chief',
        'posts_per_page' => 8
      );
      $query = new WP_Query($args);

      if ($query->have_posts()) {
      ?>
        <ul class="about-chiefs__list">
        <?php
        while ($query->have_posts()) {
          $query->the_post();
        ?>
          <li class="about-chiefs__item"><a href="<?php echo get_the_permalink(); ?>" class="target-link"></a>
            <div class="about-chiefs__image"><img src="<?php the_field('chief__image'); ?>"></div>
            <div class="about-chiefs__content">
              <h4 class="section__text about-chiefs__subtitle"><?php the_title(); ?></h4>
              <span class="section__text section__text--small about-chiefs__span"><?php the_field('chief__kitchen'); ?></span>
              <p class="section__text section__text--small about-chiefs__text"><?php the_field('chief__except'); ?></p>
              <a href="<?php echo get_the_permalink(); ?>" class="section__text section__text--small link about-chiefs__more">Подробно о шефе</a>
            </div>
          </li>
        <?php
        }
        wp_reset_postdata();
        ?>
        </ul>
      <?php
      }
      ?>
    </div>
  </section>
  <?php include(locate_template('template-parts/template-contacts.php')); ?>
</main>
<?php get_footer(); ?>
